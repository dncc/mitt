#!/bin/bash
set -e

if [ ! -e text8 ]; then
if hash wget 2>/dev/null; then
  wget http://mattmahoney.net/dc/text8.zip
else
  curl -O http://mattmahoney.net/dc/text8.zip
fi
unzip text8.zip
rm text8.zip
fi

DATA_DIR=.data
mkdir -p ${DATA_DIR}

COMB_DIR=${DATA_DIR}/combine
mkdir -p ${COMB_DIR}

DATA_FILE=${DATA_DIR}/split.text8
BIN_DIR=./target/release
COCCUR=${DATA_DIR}/cooccurrence.bin
VOC=${DATA_DIR}/vocab.txt
VEC=${DATA_DIR}/vectors.txt
NGVEC=${DATA_DIR}/ngram.vectors.txt

TMP_DIR=/tmp
ulimit -n 99999
export TMPDIR=${TMP_DIR}

${BIN_DIR}/mitt vocab -i ${DATA_FILE} -v ${VOC} -p 4

${BIN_DIR}/mitt cooccur -i ${DATA_FILE} -v ${VOC} -p 8 -o ${COCCUR}
# ${BIN_DIR}/mitt ngrams-cooccur -i ${DATA_FILE} -v ${VOC} -p 4 -o ${COCCUR}

${BIN_DIR}/mitt train -i ${COCCUR} -v ${VOC} -p 8 --vector-size 300 --max-iter 30 -o ${VEC}
# ${BIN_DIR}/mitt train -i ${COCCUR} -v ${VOC} -p 8 --vector-size 300 --max-iter 30 -o ${NGVEC}

${BIN_DIR}/mitt combine -i ${NGVEC} -v ${VOC} --out-dir ${COMB_DIR} --output-format 1

