PROJECT_LOCAL_PATH := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))
PROJECT_DIR := pweight
PROJECT_REMOTE_PATH := /raid/${USER}/${PROJECT_DIR}

.PHONY: ssh/rsync
ssh/rsync:
	ssh root@${IP} "mkdir -p ${PROJECT_REMOTE_PATH}"
	rsync -arvz \
		--exclude='.git' \
		--exclude='index' \
		--exclude='target' \
		--exclude='parts' \
		--exclude='.data' \
		--exclude='build' \
		--exclude='dist' \
		--exclude='*.egg-info' \
		--filter=': -.gitignore' \
	${PROJECT_LOCAL_PATH}/ root@${IP}:${PROJECT_REMOTE_PATH}/

.PHONY: build/scp
build/scp:
	cargo build --release
	ssh root@${IP} "mkdir -p ${PROJECT_REMOTE_PATH}"
	scp ./target/release/mitt root@${IP}:${PROJECT_REMOTE_PATH}/mitt

.PHONY: rsync/scripts
rsync/scripts:
	ssh root@${IP} "mkdir -p ${PROJECT_REMOTE_PATH}/scripts"
	rsync -arvz \
		--exclude='.git' \
		--exclude='index' \
		--exclude='target' \
		--exclude='parts' \
		--exclude='.data' \
		--exclude='build' \
		--exclude='dist' \
		--exclude='*.egg-info' \
		--filter=': -.gitignore' \
	${PROJECT_LOCAL_PATH}/scripts/ root@${IP}:${PROJECT_REMOTE_PATH}/scripts/

TMUXW := 0

.PHONY: install/py
install/py:
	ssh root@${IP} "tmux new -d -s qpick || true && tmux select-window -t qpick:${TMUXW} \
										|| tmux new-window -n ${TMUXW} && \
					tmux send -t qpick:${TMUXW} 'apt-get update && apt-get install -y libxml2-dev python3-pip' ENTER && \
					tmux send -t qpick:${TMUXW} 'cd ${PROJECT_REMOTE_PATH}' ENTER && \
 					tmux send -t qpick:${TMUXW} 'export LC_ALL="en_US.UTF-8"' ENTER && \
					tmux send -t qpick:${TMUXW} 'python3 -m pip install pip --upgrade --ignore-installed' ENTER && \
					tmux send -t qpick:${TMUXW} 'PIP_INDEX_URL=\$$PIP_INDEX_URL \
									   PIP_TRUSTED_HOST=\$$PIP_TRUSTED_HOST \
									   python3 -m pip install -v -q -r ./scripts/requirements.txt --ignore-installed' ENTER"


.PHONY: install/dep
install/dep: rsync/scripts install/py


# ================ localhost =================

BIN_NAME = mitt

.PHONY: install/rust
install/rust:
	sudo apt-get install libffi-dev
	curl https://sh.rustup.rs -sSf | sh -s -- -y
	export PATH=~/.cargo/bin:${PATH}

.PHONY: build/mitt
build/mitt:
	PATH=~/.cargo/bin:${PATH}
	cargo build

.PHONY: link/mitt
link/mitt:
	@ln -sf ./target/debug/$(BIN_NAME) .

.PHONY: release/mitt
release/mitt:
	PATH=~/.cargo/bin:${PATH}
	cargo build --release

.PHONY: install/mitt
install/mitt:
	@cp ./target/release/$(BIN_NAME) /usr/local/bin/$(BIN_NAME)

.PHONY: build
build: build/mitt link/mitt

.PHONY: install
install: release/mitt install/mitt

# ================ Docker =================
IMG     ?= neg
IMG_TAG ?= m

.PHONY: docker/build
docker/build:
	sudo docker build . -t ${PROJECT_DIR}/${IMG}:${IMG_TAG}

.PHONY: docker/scp
docker/scp:
	sudo docker save -o ${PROJECT_DIR}.${IMG}.${IMG_TAG} tar ${PROJECT_DIR}/${IMG}:${IMG_TAG} && \
	sudo chown dnc:dnc ${PROJECT_DIR}.${IMG}.${IMG_TAG}.tar && \
	scp ${PROJECT_DIR}.${IMG}.${IMG_TAG}.tar root@${IP}:${PROJECT_REMOTE_PATH} && \
	ssh root@${IP} "docker load -i ${PROJECT_REMOTE_PATH}/${PROJECT_DIR}.${IMG}.${IMG_TAG}.tar"

.PHONY: docker/run
docker/run:
	ssh root@${IP} "docker run -td -v ${PROJECT_REMOTE_PATH}:/data ${PROJECT_DIR}/${IMG}:${IMG_TAG}"

