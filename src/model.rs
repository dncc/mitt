use byteorder::{ByteOrder, LittleEndian};
use pbr::ProgressBar;
use random::{Kiss64Random, Random};
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::io::{BufReader, BufWriter, Error, SeekFrom};
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::sync::{Arc, Mutex, RwLock};
use std::thread;

use fst::raw::{Fst, MmapReadOnly};
use fst::Map;

use blas;
use cooccur;
use memstat;
use util;
use util::RAND_MAX;

use dict::{Dict, EntryType};

const ETA: f64 = 0.05; // Initial learning rate
const GAMMA: f64 = 0.9; // RMSProp exp. decaying const.
const CLIP_NORM: f64 = 200.0;
const BIAS_THRES: f64 = 20.0;
const BIAS_SCALE: f64 = BIAS_THRES / CLIP_NORM;
const TABLE_SIZE: usize = 100_000_000;
const NEG_VAL_LN: f64 = -1.386294; // ln(0.25)
const WDC_THRESH: f64 = 100.0 / 300.0;

#[inline]
pub fn norm(v: &[f64]) -> f64 {
    unsafe { blas::dnrm2(v.len() as i32, v, 1) }
}

#[inline]
pub fn clip_by_norm(v: &mut [f64], threshold: f64, norm: f64) {
    unsafe { blas::dscal(v.len() as i32, threshold / norm, v, 1) };
}

#[inline]
pub fn scale_by_min(v: &mut [f64]) {
    let min_val = v.iter().fold(std::f64::MAX, |mut min, &e| {
        if e < min {
            min = e;
        }
        min
    });

    if min_val > 1.0 {
        unsafe { blas::dscal(v.len() as i32, 1.0 / min_val, v, 1) };
    }
}

fn clip_gradient_if_needed(mut gv: &mut Vec<f64>, gv_norm: f64) -> u64 {
    if gv_norm > CLIP_NORM {
        // clip bias term separately if it's too large
        if let Some(gb) = gv.last_mut() {
            if *gb > BIAS_THRES {
                *gb = util::min(*gb * BIAS_SCALE, BIAS_THRES);
            }
        }

        if gv_norm < 2.0 * CLIP_NORM {
            scale_by_min(&mut gv);
        } else {
            for e in gv.iter_mut() {
                *e = 1.0;
            }
        }

        return 1;
    }

    return 0;
}

#[inline]
pub fn dot(v: &[f64], u: &[f64]) -> f64 {
    unsafe { blas::ddot(v.len() as i32, v, 1, u, 1) }
}

#[inline]
fn init_params<R: Random<u64>>(
    vector_size: usize,
    dict: &Arc<Dict>,
    rand: &mut R,
) -> (
    Vec<Arc<RwLock<WordVec>>>,
    Vec<Arc<RwLock<WordVec>>>,
    Arc<Vec<u32>>,
    Arc<Vec<u32>>,
) {
    let vocab_size = dict.size - 1;

    let cap = 2 * vocab_size;
    let bias_vec_size = vector_size + 1; // + 1 for bias
    let indices: Vec<usize> = (0..bias_vec_size).collect();

    let mut w: Vec<Arc<RwLock<WordVec>>> = Vec::with_capacity(cap);
    let mut gradsq: Vec<Arc<RwLock<WordVec>>> = Vec::with_capacity(cap);

    for _ in 0..2 * vocab_size {
        gradsq.push(Arc::new(RwLock::new(WordVec(vec![1.0; bias_vec_size]))));

        w.push(Arc::new(RwLock::new(WordVec(
            indices
                .iter()
                .map(|_| {
                    (rand.index(RAND_MAX as u64 + 1) as f64 / RAND_MAX - 0.5) / bias_vec_size as f64
                })
                .collect::<Vec<f64>>(),
        ))));
    }

    // init CDF table iterate over dict entries and create cumulative sum of pow probabilities
    let power: f64 = 0.75;

    // Word CDF
    let vocab_size = dict.nwords as usize;
    let pnorm: f64 = (1..=vocab_size).fold(0.0, |mut s, i| {
        s += (dict.get_entry(i).count as f64).powf(power);
        s
    });

    let mut j = 1;
    let mut words_cdf_table: Vec<u32> = vec![0; TABLE_SIZE];
    let mut d1 = (dict.get_entry(j).count as f64).powf(power) / pnorm;
    for i in 0..TABLE_SIZE {
        words_cdf_table[i] = j as u32;
        if (i as f64 / TABLE_SIZE as f64) > d1 && j < vocab_size {
            j += 1;
            d1 += (dict.get_entry(j).count as f64).powf(power) / pnorm;
        }
    }

    // Ngram CDF, not used, remove?
    let ngrams_offset = (dict.nwords + 1) as usize;
    let ngrams_size = (dict.size - 1) as usize;
    let pnorm: f64 = (ngrams_offset..=ngrams_size).fold(0.0, |mut s, i| {
        s += (dict.get_entry(i).count as f64).powf(power);
        s
    });

    let mut j = ngrams_offset;
    let mut ngrams_cdf_table: Vec<u32> = vec![0; TABLE_SIZE];
    if j < ngrams_size {
        let mut d1 = (dict.get_entry(j).count as f64).powf(power) / pnorm;
        for i in 0..TABLE_SIZE {
            ngrams_cdf_table[i] = j as u32;
            if (i as f64 / TABLE_SIZE as f64) > d1 && j < ngrams_size {
                j += 1;
                d1 += (dict.get_entry(j).count as f64).powf(power) / pnorm;
            }
        }
    }

    (
        w,
        gradsq,
        Arc::new(words_cdf_table),
        Arc::new(ngrams_cdf_table),
    )
}

#[inline]
fn get_file_len(path: &str) -> u64 {
    let metadata = std::fs::metadata(path).unwrap();

    metadata.len() / cooccur::Crec::get_size() as u64
}

#[inline]
fn check_nan(v: f64) -> f64 {
    if v.is_nan() || v.is_infinite() {
        return 0.0;
    }

    v
}

#[derive(Clone, Copy, Debug)]
pub enum SaveMode {
    WithBias,
    WoutBias,
}

impl From<u8> for SaveMode {
    fn from(u: u8) -> Self {
        match u {
            0 => SaveMode::WithBias,
            1 => SaveMode::WoutBias,
            _ => panic!("Invalid value for the save mode {:?}!", u),
        }
    }
}

impl From<SaveMode> for String {
    fn from(save_mode: SaveMode) -> String {
        match save_mode {
            SaveMode::WithBias => String::from("with bias"),
            SaveMode::WoutBias => String::from("without bias"),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum VerboseMode {
    Info,
    Debug,
}

impl From<u8> for VerboseMode {
    fn from(u: u8) -> Self {
        match u {
            0 => VerboseMode::Info,
            1 => VerboseMode::Debug,
            _ => panic!("Invalid value for verbose mode {:?}!", u),
        }
    }
}

impl From<VerboseMode> for String {
    fn from(verbose_mode: VerboseMode) -> String {
        match verbose_mode {
            VerboseMode::Info => String::from("INFO"),
            VerboseMode::Debug => String::from("DEBUG"),
        }
    }
}

fn save_txt(
    word_vectors: &Vec<Arc<RwLock<WordVec>>>,
    vector_size: usize,
    dict: &Arc<Dict>,
    output_file_path: &str,
    save_mode: SaveMode,
) {
    println!(
        "Saving parameters to: {:?}, {}.",
        output_file_path,
        String::from(save_mode)
    );

    let word_vec_dim: usize = match save_mode {
        SaveMode::WithBias => vector_size + 1,
        SaveMode::WoutBias => vector_size,
    };

    util::rm_when_exist(output_file_path);
    let file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(output_file_path)
        .unwrap();
    let mut writer = BufWriter::new(file);

    let vocab_size = dict.size - 1;
    for i in 0..vocab_size {
        // word ids start from 1
        let entry = dict.get_entry(i + 1);
        // if ngrams are not generated, save word without BOW & EOW labels
        let word = if dict.nngrams == 0 {
            entry.get_word().unwrap().clone()
        } else {
            entry.token.clone()
        };

        let mut data = String::with_capacity(word.len() + 8 * word_vec_dim);
        data.push_str(&word);

        // begin for word, with the bias term
        let wi = &word_vectors[i].read().unwrap().0[..word_vec_dim];
        // begin for context, with the bias term
        let wj = &word_vectors[vocab_size + i].read().unwrap().0[..word_vec_dim];

        data = wi.iter().zip(wj).fold(data, |mut s, (vi, vj)| {
            s.push_str(&format!(" {:.6}", vi + vj));
            s
        });
        data.push_str("\n");

        writer
            .write_all(data.as_bytes())
            .expect("Unable to write data");
    }
}

fn print_train_info(
    words_pre_skip: u64,
    words_skip_num: u64,
    words_post_skip: u64,
    ngrams_pre_skip: u64,
    ngrams_skip_num: u64,
    ngrams_post_skip: u64,
    word_negatives: u64,
    num_decay: u64,
) {
    println!("\nWords num: {}", words_pre_skip);
    println!("Words skipped: {}", words_skip_num);
    println!("Ngrams num: {}", ngrams_pre_skip);
    println!("Ngrams skipped: {}", ngrams_skip_num);
    println!(
        "Word post-skip: {}, wrt pre-skip: {}%",
        words_post_skip,
        (100.0 * words_post_skip as f64 / words_pre_skip as f64).round()
    );
    println!(
        "Word negatives: {}, wrt post-skip: {}%, wrt pre-skip: {}%",
        word_negatives,
        (100.0 * word_negatives as f64 / words_post_skip as f64).round(),
        (100.0 * word_negatives as f64 / words_pre_skip as f64).round()
    );
    let word_positives = words_post_skip - word_negatives;
    println!(
        "Word positives: {}, wrt post-skip: {}%, wrt pre-skip: {}%",
        word_positives,
        (100.0 * word_positives as f64 / words_post_skip as f64).round(),
        (100.0 * word_positives as f64 / words_pre_skip as f64).round()
    );
    println!(
        "Ngram post-skip: {}, wrt pre-skip: {}%",
        ngrams_post_skip,
        (100.0 * ngrams_post_skip as f64 / ngrams_pre_skip as f64).round()
    );
    println!("Decayed weights: {}", num_decay);
    println!("----");
}

#[inline]
fn select_negative<R: Random<u64>>(
    cdf: &Vec<u32>,
    wi: usize,
    wk: usize,
    vivk: f64,
    val_ik_ln: f64,
    rw_w: &Vec<Arc<RwLock<WordVec>>>,
    mut vj: &mut Vec<f64>,
    vk: &Vec<f64>,
    bias_vec_size: usize,
    dict_size: usize,
    map: &Arc<fst::Map>,
    neg_val_ln: f64,
    trials: usize,
    cooccur_thresh: f64,
    rand: &mut R,
) -> (usize, f64, f64, u64) {
    let mut vjvk: f64 = 0.0;
    let (mut lj, mut val_jk_ln, mut is_negative) = (0, neg_val_ln, 0); // TODO hyper param,0.1

    if val_ik_ln < cooccur_thresh {
        return (lj, val_jk_ln, vjvk, is_negative);
    }

    let mut j = 0;
    while j < trials {
        let j_neg = cdf[rand.index(TABLE_SIZE as u64) as usize] as usize;
        if j_neg == wi || j_neg == wk {
            continue;
        }

        lj = j_neg - 1;
        let wjwk_id = cooccur::lookup_idx(j_neg, wk as usize, dict_size).to_string();
        if !map.contains_key(&wjwk_id) {
            val_jk_ln = neg_val_ln;
            unsafe {
                let gl_vj = rw_w[lj].read().unwrap();
                blas::dcopy(bias_vec_size as i32, &gl_vj.0, 1i32, &mut vj, 1i32);
            }
            vjvk = dot(&vk, &vj);
            if vjvk > vivk - 0.5 * vivk.abs() {
                is_negative = 1;
                return (lj, val_jk_ln, vjvk, is_negative);
            }
        }
        j += 1;
    }

    return (lj, val_jk_ln, vjvk, is_negative);
}

/*

 Glove minimizes the following cost function:

    J = Σ ᵢ,ₖ f(xᵢ,ₖ) (bᵢ + bₖ + W'ᵢWₖ - logₑ(Xᵢ,ₖ))²

 Here, it is changed into:

    J = Σ ᵢ,ₖ f(xᵢ,ₖ) (bᵢ - bₗ + W'ᵢWₖ - logₑ(Xᵢ,ₖ) - W'ᵢWₗ + logₑ(Xᵢ,ₗ))²

 where W'ᵢ is transposed weight vector for i-th word, Wₖ is weight vector for
 its context k-th word, Wₗ is weigth vector for word that is a negative sample
 for a context word Wₖ, such that i, k go from 1 to V, V is the size of the
 vocabulary and f(xᵢ,ₖ) is defined as follows:

                /
                |  (x / xₘₐₓ)^α , if x < xₘₐₓ
        f(x) = <
                |  1 , otherwise.
                \


 where xₘₐₓ and α are hand picked function parameters, not extremely sensitive
 to corpus, though may need adjustment for very small or very large corpora.
 Default values are 100 and 0.75 respectively.

 For more details see: https://nlp.stanford.edu/pubs/glove.pdf

*/

struct WordVec(Vec<f64>);

#[inline]
fn train_iter(
    iter: usize,
    rw_w: &Vec<Arc<RwLock<WordVec>>>,
    rw_gradsq: &Vec<Arc<RwLock<WordVec>>>,
    records_per_worker: &Vec<(u64, u64)>,
    input_file_path: &str,
    map: &Arc<fst::Map>,
    dict: &Arc<Dict>,
    words_cdf: &Arc<Vec<u32>>,
    xmax: f64,
    vector_size: usize,
    alpha: f64,
    eta: f64,    // learning rate
    lambda: f64, // regularization rate
    num_workers: usize,
    num_records: u64,
    verbose_mode: VerboseMode,
    neg_trials: usize,
    neg_min_freq: f64,
) -> Result<(f64, u64, u64, u64), Error> {
    let (sender, receiver): (Sender<(f64, u64, u64, u64)>, Receiver<(f64, u64, u64, u64)>) =
        mpsc::channel();

    let pb = Arc::new(Mutex::new(ProgressBar::new(
        (num_records / 1_000_000) as u64 + 1,
    )));
    pb.lock().unwrap().inc();

    let dict_size = dict.size;
    let vocab_size = dict_size - 1;

    for worker_id in 0..num_workers {
        let pb = pb.clone();
        let mut rw_w = rw_w.clone();
        let mut rw_gradsq = rw_gradsq.clone();
        let words_cdf = words_cdf.clone();
        let sender = sender.clone();
        let input_file_path = input_file_path.to_string().clone();
        let records_per_worker = records_per_worker.clone();
        let map = map.clone();
        let dict = dict.clone();
        let cooccur_file = OpenOptions::new()
            .read(true)
            .open(&input_file_path)
            .expect(&format!("Failed to open file: {:?}", &input_file_path));

        thread::spawn(move || {
            let mut cost: f64 = 0.0;
            let word_size = cooccur::Crec::get_word_size();

            let addr = records_per_worker[worker_id].0;
            // The buffer's size should be a multiple of crec size, or the reader only partially
            // reads the last in the buffer.
            let mut reader =
                BufReader::with_capacity(cooccur::Crec::get_size() * memstat::MB, cooccur_file);
            reader.seek(SeekFrom::Start(addr)).unwrap();

            let mut min_clip_num: u64 = 0;
            let mut rms_clip_num: u64 = 0;

            let mut words_pre_skip: u64 = 0;
            let mut words_skip_num: u64 = 0;
            let mut words_post_skip: u64 = 0;
            let mut ngrams_pre_skip: u64 = 0;
            let mut ngrams_skip_num: u64 = 0;
            let mut ngrams_post_skip: u64 = 0;

            let mut num_decay: u64 = 0;
            let mut word_negatives: u64 = 0;
            let neg_min_freq_ln = neg_min_freq.ln();

            let mut diff_nan_num: u64 = 0;
            let mut buf = vec![0u8; cooccur::Crec::get_size()];

            // initialize vectors, gradients and updates
            let bias_vec_size = vector_size + 1;
            let weight_thresh = WDC_THRESH * bias_vec_size as f64;

            let mut bi: f64;
            let mut bk: f64;
            let mut bj: f64 = 0.0;

            let mut gbi: f64;
            let mut gbk: f64;
            let mut gbj: f64;

            let mut vi: Vec<f64> = vec![0.0; bias_vec_size];
            let mut vk: Vec<f64> = vec![0.0; bias_vec_size];
            let mut vj: Vec<f64> = vec![0.0; bias_vec_size];

            let mut gvi: Vec<f64> = vec![0.0; bias_vec_size];
            let mut gvk: Vec<f64> = vec![0.0; bias_vec_size];
            let mut gvj: Vec<f64> = vec![0.0; bias_vec_size];

            let mut w_updates_i: Vec<f64> = vec![0.0; vector_size];
            let mut w_updates_k: Vec<f64> = vec![0.0; vector_size];
            let mut w_updates_j: Vec<f64> = vec![0.0; vector_size];

            let mut rand = Kiss64Random::new(Some((iter * num_workers + worker_id) as u64));

            let update_w_fns: [Box<dyn Fn(f64, f64, f64, f64) -> (f64, f64)>; 2] = [
                // without weight decay
                Box::new(
                    #[inline(always)]
                    |fdiff, vk, _lambda, _vi| (fdiff * vk, 0.0),
                ),
                // with weight decay
                Box::new(
                    #[inline(always)]
                    |fdiff, vk, lambda, vi| (fdiff * vk, lambda * vi),
                ),
            ];

            let update_gradsq_fns: [Box<dyn Fn(f64, f64) -> f64>; 2] = [
                // Adagrad update
                Box::new(
                    #[inline(always)]
                    |gradsq, deltagrad| gradsq + deltagrad * deltagrad,
                ),
                // RMSProp update
                Box::new(
                    #[inline(always)]
                    |gradsq, deltagrad| GAMMA * gradsq + (1.0 - GAMMA) * deltagrad * deltagrad,
                ),
            ];

            for count in 0..records_per_worker[worker_id].1 {
                let bytes_read = reader.read(&mut buf).unwrap();
                assert!(
                    bytes_read == cooccur::Crec::get_size(),
                    "Read {:?} bytes, expected {:?}",
                    bytes_read,
                    cooccur::Crec::get_size()
                );

                if (count + 1) % 1_000_000 == 0 {
                    pb.lock().unwrap().inc();
                }

                let crec = cooccur::Crec {
                    w1: LittleEndian::read_u32(&buf[0..word_size]),
                    w2: LittleEndian::read_u32(&buf[word_size..2 * word_size]),
                    val: LittleEndian::read_f32(&buf[2 * word_size..]),
                };

                let wi = crec.w1 as usize;
                let wk = crec.w2 as usize;
                let val_ik = crec.val as f64;
                let val_ik_ln = val_ik.ln();

                // crec word indices start at 1
                if wi < 1 || wi > vocab_size || wk < 1 || wk > vocab_size {
                    panic!(
                        "Word ids (wi: {}, wk: {}) are out of the range: [1, {}]",
                        wi, wk, vocab_size
                    );
                };

                let ei = dict.get_entry(wi);
                let ek = dict.get_entry(wk);

                let (nt, pmi_thresh, skip_num) = if ei.token_type == EntryType::WORD {
                    words_pre_skip += 1;
                    (dict.nword_tokens as f64, 1.0, &mut words_skip_num)
                } else {
                    ngrams_pre_skip += 1;
                    (dict.nngram_tokens as f64, 2.0, &mut ngrams_skip_num)
                };

                let (ci, ck) = (ei.count as f64, ek.count as f64);

                let pmi = val_ik / ci / ck * nt;
                if pmi < pmi_thresh {
                    *skip_num += 1;
                    continue;
                }

                // Get vector indexes in W & gradsq
                let li = wi - 1;
                // shift by vocab_size to get separate vector for the context word
                let lk = wk - 1 + vocab_size;

                /* Calculate cost, save diff for gradients */
                unsafe {
                    let gl_vi = rw_w.get_unchecked(li).read().unwrap();
                    blas::dcopy(bias_vec_size as i32, &gl_vi.0, 1i32, &mut vi, 1i32);
                }
                bi = vi.pop().unwrap();

                unsafe {
                    let gl_vk = rw_w.get_unchecked(lk).read().unwrap();
                    blas::dcopy(bias_vec_size as i32, &gl_vk.0, 1i32, &mut vk, 1i32);
                }
                bk = vk.pop().unwrap();

                let vivk = dot(&vi, &vk);

                let (lj, val_jk_ln, vjvk, is_negative) = if ei.token_type == EntryType::NGRAM {
                    ngrams_post_skip += 1;
                    (0, 0.0, 0.0, 0)
                } else {
                    words_post_skip += 1;
                    let (neg_val_ln, cdf) = (NEG_VAL_LN, &words_cdf);

                    select_negative(
                        cdf,
                        wi,
                        wk,
                        vivk,
                        val_ik_ln,
                        &rw_w,
                        &mut vj,
                        &vk,
                        bias_vec_size,
                        dict_size,
                        &map,
                        neg_val_ln,
                        neg_trials,
                        neg_min_freq_ln,
                        &mut rand,
                    )
                };

                if is_negative == 1 {
                    word_negatives += 1;
                }

                let diff = if is_negative > 0 {
                    bj = vj.pop().unwrap();
                    // bias for each word, and dot product of word- and context-word vectors
                    bi - bj + vivk - val_ik_ln - vjvk + val_jk_ln
                } else {
                    bi + bk + vivk - val_ik_ln
                };

                if diff.is_nan() || diff.is_infinite() {
                    diff_nan_num += 1;
                    vi.push(bi);
                    if is_negative > 0 {
                        vj.push(bj);
                    }
                    vk.push(bk);

                    continue;
                };

                let fx = if val_ik >= xmax {
                    1.0
                } else {
                    (val_ik / xmax).powf(alpha)
                };

                // multiply weighting function (f) with diff
                let mut fdiff = fx * diff;

                cost += 0.5 * fdiff as f64 * diff as f64;
                // Include learning rate into Adagrad's RSS gradient
                fdiff *= eta;

                // Adaptive gradient updates,
                unsafe {
                    let grad_vi = rw_gradsq.get_unchecked(li).read().unwrap();
                    blas::dcopy(bias_vec_size as i32, &grad_vi.0, 1i32, &mut gvi, 1i32);
                }
                let gvi_norm = norm(&gvi);
                min_clip_num += clip_gradient_if_needed(&mut gvi, gvi_norm);
                gbi = gvi.pop().unwrap();

                unsafe {
                    let grad_vk = rw_gradsq.get_unchecked(lk).read().unwrap();
                    blas::dcopy(bias_vec_size as i32, &grad_vk.0, 1i32, &mut gvk, 1i32);
                }
                let gvk_norm = norm(&gvk);
                min_clip_num += clip_gradient_if_needed(&mut gvk, gvk_norm);
                gbk = gvk.pop().unwrap();

                let rms_prop_i = (gvi_norm > GAMMA * CLIP_NORM) as usize;
                rms_clip_num += rms_prop_i as u64;
                let update_gradsq_i = &update_gradsq_fns[rms_prop_i];

                let rms_prop_k = (gvk_norm > GAMMA * CLIP_NORM) as usize;
                rms_clip_num += rms_prop_k as u64;
                let update_gradsq_k = &update_gradsq_fns[rms_prop_k];

                let mut w_updates_i_sum: f64 = 0.0;
                let mut w_updates_k_sum: f64 = 0.0;
                let mut w_updates_j_sum: f64 = 0.0;

                if is_negative > 0 {
                    // adaptive parameter updates
                    let vivi = dot(&vi, &vi);
                    let vkvk = dot(&vk, &vk);
                    let vjvj = dot(&vj, &vj);

                    let (i_dec, k_dec, j_dec) = (
                        vivi > weight_thresh,
                        vkvk > weight_thresh,
                        vjvj > weight_thresh,
                    );

                    let (i_dec, k_dec, j_dec) = (i_dec as usize, k_dec as usize, j_dec as usize);
                    num_decay += (i_dec + k_dec + j_dec) as u64;

                    let update_wi_fn = &update_w_fns[i_dec];
                    let update_wk_fn = &update_w_fns[k_dec];
                    let update_wj_fn = &update_w_fns[j_dec];

                    unsafe {
                        let grad_vj = rw_gradsq.get_unchecked(lj).read().unwrap();
                        blas::dcopy(bias_vec_size as i32, &grad_vj.0, 1i32, &mut gvj, 1i32);
                    }
                    let gvj_norm = norm(&gvj);
                    min_clip_num += clip_gradient_if_needed(&mut gvj, gvj_norm);
                    gbj = gvj.pop().unwrap();

                    let rms_prop_j = (gvj_norm > GAMMA * CLIP_NORM) as usize;
                    rms_clip_num += rms_prop_j as u64;
                    let update_gradsq_j = &update_gradsq_fns[rms_prop_j];

                    unsafe {
                        for b in 0..vector_size {
                            // learning rate times gradient for word vectors
                            let vib = vi.get_unchecked(b);
                            let vkb = vk.get_unchecked(b);
                            let vjb = vj.get_unchecked(b);

                            let (tmpi, decayi) = update_wi_fn(fdiff, *vkb, lambda, *vib);
                            let (tmpk, decayk) = update_wk_fn(fdiff, vib - vjb, lambda, *vkb);
                            let (tmpj, decayj) = update_wj_fn(-fdiff, *vkb, lambda, *vjb);

                            // adaptive updates
                            let gvib = gvi.get_unchecked_mut(b);
                            let wupi = w_updates_i.get_unchecked_mut(b);
                            *wupi = (tmpi / gvib.sqrt()) as f64 + decayi;

                            let gvkb = gvk.get_unchecked_mut(b);
                            let wupk = w_updates_k.get_unchecked_mut(b);
                            *wupk = (tmpk / gvkb.sqrt()) as f64 + decayk;

                            let gvjb = gvj.get_unchecked_mut(b);
                            let wupj = w_updates_j.get_unchecked_mut(b);
                            *wupj = (tmpj / gvjb.sqrt()) as f64 + decayj;

                            w_updates_i_sum += *wupi;
                            w_updates_k_sum += *wupk;
                            w_updates_j_sum += *wupj;

                            *gvib = update_gradsq_i(*gvib, tmpi);
                            *gvkb = update_gradsq_k(*gvkb, tmpk);
                            *gvjb = update_gradsq_j(*gvjb, tmpj);
                        }

                        if !w_updates_j_sum.is_nan() && !w_updates_j_sum.is_infinite() {
                            blas::daxpy(vector_size as i32, -1f64, &w_updates_j, 1, &mut vj, 1);
                        }
                    }

                    bi -= check_nan(fdiff / gbi.sqrt());
                    gbi = update_gradsq_i(gbi, fdiff);
                    bj -= check_nan(-fdiff / gbj.sqrt());
                    gbj = update_gradsq_j(gbj, -fdiff);

                    vj.push(bj);
                    unsafe {
                        let mut gl_vj = rw_w.get_unchecked_mut(lj).write().unwrap();
                        blas::dcopy(bias_vec_size as i32, &vj, 1i32, &mut gl_vj.0, 1i32);
                    }
                    gvj.push(gbj);
                    unsafe {
                        let mut grad_vj = rw_gradsq.get_unchecked_mut(lj).write().unwrap();
                        blas::dcopy(bias_vec_size as i32, &gvj, 1i32, &mut grad_vj.0, 1i32);
                    }
                } else {
                    let vivi = dot(&vi, &vi);
                    let vkvk = dot(&vk, &vk);

                    let (i_dec, k_dec) = (vivi > weight_thresh, vkvk > weight_thresh);

                    let (i_dec, k_dec) = (i_dec as usize, k_dec as usize);
                    num_decay += (i_dec + k_dec) as u64;

                    let update_wi_fn = &update_w_fns[i_dec];
                    let update_wk_fn = &update_w_fns[k_dec];

                    unsafe {
                        for b in 0..vector_size {
                            // learning rate times gradient for word vectors
                            let vib = vi.get_unchecked(b);
                            let vkb = vk.get_unchecked(b);

                            let (tmpi, decayi) = update_wi_fn(fdiff, *vkb, lambda, *vib);
                            let (tmpk, decayk) = update_wk_fn(fdiff, *vib, lambda, *vkb);

                            // adaptive updates
                            let gvib = gvi.get_unchecked_mut(b);
                            let wupi = w_updates_i.get_unchecked_mut(b);
                            *wupi = (tmpi / gvib.sqrt()) as f64 + decayi;

                            let gvkb = gvk.get_unchecked_mut(b);
                            let wupk = w_updates_k.get_unchecked_mut(b);
                            *wupk = (tmpk / gvkb.sqrt()) as f64 + decayk;

                            w_updates_i_sum += *wupi;
                            w_updates_k_sum += *wupk;
                        }
                    }

                    bi -= check_nan(fdiff / gbi.sqrt());
                    gbi = update_gradsq_i(gbi, fdiff);
                    bk -= check_nan(fdiff / gbk.sqrt());
                    gbk = update_gradsq_k(gbk, fdiff);
                }

                if !w_updates_i_sum.is_nan()
                    && !w_updates_i_sum.is_infinite()
                    && !w_updates_k_sum.is_nan()
                    && !w_updates_k_sum.is_infinite()
                {
                    unsafe {
                        blas::daxpy(vector_size as i32, -1f64, &w_updates_i, 1, &mut vi, 1);
                        blas::daxpy(vector_size as i32, -1f64, &w_updates_k, 1, &mut vk, 1);
                    }
                }

                // write weights
                vi.push(bi);
                unsafe {
                    let mut gl_vi = rw_w.get_unchecked_mut(li).write().unwrap();
                    blas::dcopy(bias_vec_size as i32, &vi, 1i32, &mut gl_vi.0, 1i32);
                }

                vk.push(bk);
                unsafe {
                    let mut gl_vk = rw_w.get_unchecked_mut(lk).write().unwrap();
                    blas::dcopy(bias_vec_size as i32, &vk, 1i32, &mut gl_vk.0, 1i32);
                }

                // write gradients
                gvi.push(gbi);
                unsafe {
                    let mut grad_vi = rw_gradsq.get_unchecked_mut(li).write().unwrap();
                    blas::dcopy(bias_vec_size as i32, &gvi, 1i32, &mut grad_vi.0, 1i32);
                }

                gvk.push(gbk);
                unsafe {
                    let mut grad_vk = rw_gradsq.get_unchecked_mut(lk).write().unwrap();
                    blas::dcopy(bias_vec_size as i32, &gvk, 1i32, &mut grad_vk.0, 1i32);
                }
            }

            if verbose_mode == VerboseMode::Debug {
                print_train_info(
                    words_pre_skip,
                    words_skip_num,
                    words_post_skip,
                    ngrams_pre_skip,
                    ngrams_skip_num,
                    ngrams_post_skip,
                    word_negatives,
                    num_decay,
                );
            }

            sender
                .send((cost, diff_nan_num, min_clip_num, rms_clip_num))
                .unwrap(); // thread finished
        });
    }

    // wait for workers to finish
    let mut total_nans: u64 = 0;
    let mut total_cost: f64 = 0.0;
    let mut total_min_clip: u64 = 0;
    let mut total_rms_clip: u64 = 0;
    for _ in 0..num_workers {
        let (cost, nans, min_clip, rms_clip) = receiver.recv().unwrap();
        total_cost += cost;
        total_nans += nans;
        total_min_clip += min_clip;
        total_rms_clip += rms_clip;
    }

    pb.lock().unwrap().finish();

    Ok((total_cost, total_nans, total_min_clip, total_rms_clip))
}

pub fn should_stop_and_save(
    iter: usize,
    max_iter: usize,
    inc_iter: usize,
    nans: u64,
    costs: &Vec<f64>,
) -> (bool, bool) {
    if iter == (max_iter - 1) {
        return (true, true);
    }

    if nans > 0 {
        println!("Exit without saving.");
        return (true, false);
    }

    if iter - inc_iter > 2 {
        if costs[iter] > costs[iter - 1] && costs[iter - 1] > costs[iter - 2] {
            return (true, true);
        }
    }

    return (false, false);
}

pub fn train(
    input_file_path: &str,
    dict: Arc<Dict>,
    xmax: f32,
    vector_size: usize,
    max_iter: usize,
    alpha: f32,
    lambda: f32,
    num_workers: usize,
    out_file_path: &str,
    save_mode: SaveMode,
    verbose_mode: VerboseMode,
    neg_trials: usize,
    neg_min_freq: f64,
) -> Result<(), Error> {
    let mut records_per_worker: Vec<(u64, u64)> = Vec::with_capacity(num_workers);
    let num_records = get_file_len(input_file_path);
    let num_per_worker = num_records / num_workers as u64;
    for worker_id in 0..num_workers - 1 {
        let addr = num_per_worker * worker_id as u64 * cooccur::Crec::get_size() as u64;
        records_per_worker.push((addr, num_per_worker));
    }
    let addr = num_per_worker * (num_workers - 1) as u64 * cooccur::Crec::get_size() as u64;
    records_per_worker.push((addr, num_per_worker + num_records % num_workers as u64));

    let map_file = MmapReadOnly::open_path(format!("{}.{}", input_file_path, &"fst")).unwrap();
    unsafe {
        util::advise_ram(map_file.as_slice())
            .expect(&format!("Advisory failed for: {}", input_file_path))
    };
    let map = match Fst::from_mmap(map_file) {
        Ok(fst) => Arc::new(Map::from(fst)),
        Err(_) => panic!("Failed to load: {}!", input_file_path),
    };
    assert!(
        num_records == map.len() as u64,
        "Map len: {} and number of records {} don't match",
        map.len(),
        num_records
    );

    println!(
        "Training with {:?} records and {:?} workers.",
        num_records, num_workers
    );

    // let vocab_size = dict.size - 1;
    let (rw_w, rw_gradsq, words_cdf, _ngrams_cdf) =
        init_params(vector_size, &dict, &mut Kiss64Random::new(None));
    println!("Model weights size {:?}.", rw_w.len());

    let mut eta = ETA;
    let mut inc_iter = 0;
    let mut costs: Vec<f64> = vec![0.0; max_iter];
    let mut delta_cost_thresh = 0.05;
    let delta_cost_mult = 0.5;

    for i in 0..max_iter {
        util::shuffle(
            &mut records_per_worker,
            &mut Kiss64Random::new(Some(i as u64)),
        );

        let (cost, nans, min_clip, rms_clip) = train_iter(
            i,
            &rw_w,
            &rw_gradsq,
            &records_per_worker,
            input_file_path,
            &map,
            &dict,
            &words_cdf,
            xmax as f64,
            vector_size,
            alpha as f64,
            eta as f64,
            lambda as f64,
            num_workers,
            num_records as u64,
            verbose_mode,
            neg_trials,
            neg_min_freq,
        )
        .unwrap();

        costs[i] = cost / num_records as f64;
        let delta_cost = if i == 0 {
            0.0
        } else {
            (costs[i - 1] - costs[i]) / costs[i - 1]
        };

        println!(
            "iter: {:>2}, eta: {:.2}, cost: {:.4}, delta: {:>6}%, nan: {}, min clip: {:>4}, rms clip: {:>4}",
            i + 1,
            eta,
            costs[i],
            (10_000.0 * delta_cost).round() / 100.0,
            nans,
            min_clip,
            rms_clip,
        );

        let (should_stop, should_save) = should_stop_and_save(i, max_iter, inc_iter, nans, &costs);

        if i - inc_iter > 3 && delta_cost < delta_cost_thresh {
            eta = util::min(0.05, eta + 0.01);
            delta_cost_thresh = util::max(0.007, delta_cost_mult * delta_cost_thresh);
            inc_iter = i;
        }

        if should_save {
            save_txt(&rw_w, vector_size, &dict, out_file_path, save_mode);
        }

        if should_stop {
            break;
        }
    }

    Ok(())
}
