use flate2::read::GzDecoder;
use rand::distributions::Alphanumeric;
use rand::Rng;
use random::Random;
use std::env::temp_dir;
use std::ffi::OsStr;
use std::fs;
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::str;

use libc;
use memstat;

/// Read normal or compressed files, look for `.gz` extension to decide
pub fn reader(filename: &str) -> Box<dyn BufRead> {
    let path = Path::new(filename);
    let file = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", path.display(), why),
        Ok(file) => file,
    };

    if path.extension() == Some(OsStr::new("gz")) {
        Box::new(BufReader::with_capacity(
            128 * 1024,
            GzDecoder::new(file).unwrap(),
        ))
    } else {
        Box::new(BufReader::with_capacity(128 * 1024, file))
    }
}

pub fn tmp_file_path(prefix: &str, suffix: &str, rand_len: usize) -> String {
    let mut buf = String::with_capacity(prefix.len() + suffix.len() + rand_len);
    buf.push_str(prefix);
    unsafe {
        rand::thread_rng()
            .sample_iter(&Alphanumeric)
            .take(rand_len)
            .for_each(|b| buf.push_str(str::from_utf8_unchecked(&[b as u8])))
    }
    buf.push_str(suffix);

    temp_dir().join(buf).into_os_string().into_string().unwrap()
}

#[inline]
pub fn max<T: PartialOrd>(a: T, b: T) -> T {
    if a > b {
        a
    } else {
        b
    }
}

#[inline]
pub fn min<T: PartialOrd>(a: T, b: T) -> T {
    if a < b {
        a
    } else {
        b
    }
}

use pbr::ProgressBar;
use std::io::Stdout;

pub fn init_pb(input_file: &str, mult: Option<usize>) -> ProgressBar<Stdout> {
    let mult = mult.unwrap_or(1) as u64;
    let metadata = std::fs::metadata(input_file).unwrap();
    let len = mult * (metadata.len() as f32 / memstat::MB as f32).ceil() as u64;

    ProgressBar::new(len)
}

pub fn rm_when_exist(out_file_name: &str) {
    let metadata = std::fs::metadata(out_file_name);
    if metadata.is_ok() {
        fs::remove_file(out_file_name)
            .expect(&format!("Failed to delete file: {:?}", out_file_name));
    }
}

pub fn shuffle<R: Random<u64>, T: Copy>(elements: &mut Vec<T>, rand: &mut R) {
    for i in (2..elements.len()).rev() {
        let j = rand.index(i as u64) as usize;
        let tmp = elements[j];
        elements[j] = elements[i];
        elements[i] = tmp;
    }
}

pub const RAND_MAX: f64 = 2147483647.0; // 2 ^ 31 - 1

/*
   Sub-sampling

   Randomly remove words that are more frequent than some threshold `t`
   with a probability `p`, where `f` marks the word's corpus frequency:

        p = 1 - √ (t / f)

    https://levyomer.files.wordpress.com/2015/03/improving-distributional-similarity-tacl-2015.pdf

*/

pub const THRESHOLD: f64 = 0.001;

#[inline]
pub fn skip_rnd<R: Random<u64>>(p: f64, count_sum: f64, rand: &mut R) -> bool {
    if p <= 0.0 {
        return false;
    }
    let r = rand.index(count_sum as u64 + 1) as f64 / count_sum;

    r < p
}

#[inline]
pub fn skip_random<R: Random<u64>>(f: f64, count_sum: f64, rand: &mut R) -> bool {
    let f = f / count_sum;
    if f < THRESHOLD {
        return false;
    }

    let p = 1.0 - (THRESHOLD / f).sqrt();
    let r = rand.index(count_sum as u64 + 1) as f64 / count_sum;

    r < p
}

// Replace original distance weights (harmonic) (3.1) with this:
// https://levyomer.files.wordpress.com/2015/03/improving-distributional-similarity-tacl-2015.pdf
#[inline]
pub fn dist_weight(window_size: usize, j: usize, k: i32) -> f32 {
    (window_size - (j - k as usize - 1)) as f32 / window_size as f32
}

#[inline]
pub fn as_ptr<T>(v: &[T]) -> *const T {
    v as *const [T] as *const T
}

// Advise the OS on the random access pattern of data.
// Taken from https://docs.rs/crate/madvise/0.1.0
#[cfg(unix)]
pub fn advise_ram(data: &[u8]) -> io::Result<()> {
    unsafe {
        let result = libc::madvise(
            as_ptr(data) as *mut libc::c_void,
            data.len(),
            libc::MADV_RANDOM as libc::c_int,
        );

        if result == 0 {
            Ok(())
        } else {
            Err(io::Error::last_os_error())
        }
    }
}
