use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::sync::{Arc, Mutex};
use std::thread;

use fnv::{FnvHashMap, FnvHashSet};
use pbr::ProgressBar;
use std::cmp::{Ordering, PartialOrd};
use std::fs::{File, OpenOptions};
use std::io::prelude::*;
use std::io::Stdout;
use std::io::{BufWriter, Error};
use std::path::Path;

use random::{Kiss64Random, Random};

use memstat;
use util;

pub const VOCAB_SIZE: usize = 350_000_000; // max # of entries in the vocabulary words or word-ngrams

#[derive(Clone, Debug)]
struct Shard {
    id: usize,
    data: FnvHashMap<String, u32>,
}

fn learn_vocab_thread(
    input_file: String,
    worker_id: usize,
    num_workers: usize,
    pb: Arc<Mutex<ProgressBar<Stdout>>>,
) -> (FnvHashMap<String, (u32, bool)>, u64, u64) {
    let vocab_size = VOCAB_SIZE / num_workers;

    let mut proc_data: usize = 0;
    let mut curr_line: u64 = 0;
    let mut line_count: u64 = 0;
    let mut word_count: u64 = 0;
    let mut down_size_freq: u32 = 1;
    let mut vocab: FnvHashMap<String, (u32, bool)> = FnvHashMap::default();

    // initialized out of the read lines loop to carry out the prev_word between 2 lines
    let mut prev_word: String = "".to_string();
    let reader = util::reader(&input_file);
    for line in reader.lines() {
        curr_line += 1;
        if curr_line % num_workers as u64 != worker_id as u64 {
            continue;
        }

        line_count += 1;
        let line = line.unwrap();
        let words: Vec<String> = line
            .split(" ")
            .filter(|w| w.len() > 0)
            .map(|w| w.to_string())
            .collect();
        word_count += words.len() as u64;
        for word in words {
            if prev_word != "" {
                let mut phrase = String::with_capacity(word.len() + prev_word.len() + 1);
                phrase.push_str(&prev_word);
                phrase.push_str("_");
                phrase.push_str(&word);
                let (big_count, _is_phrase) = vocab.entry(phrase).or_insert((0, true));
                *big_count += 1;
            }

            prev_word = word.clone();
            let (count, _is_phrase) = vocab.entry(word).or_insert((0, false));
            *count += 1;
        }

        proc_data += line.len();
        if proc_data > memstat::MB {
            pb.lock().unwrap().inc();
            proc_data -= memstat::MB;
            // check size and resize if needed
            if vocab.len() > vocab_size {
                vocab = vocab
                    .into_iter()
                    .filter(|(_word, (count, _is_phrase))| *count > down_size_freq)
                    .collect();
                down_size_freq += 1;
            }
        }
    }

    (vocab, word_count, line_count)
}

pub fn learn_vocab(
    input_file: &str,
    min_freq: u32,
    num_workers: usize,
) -> (FnvHashMap<String, (u32, bool)>, u64, u64) {
    let (sender, receiver): (
        Sender<(FnvHashMap<String, (u32, bool)>, u64, u64)>,
        Receiver<(FnvHashMap<String, (u32, bool)>, u64, u64)>,
    ) = mpsc::channel();

    let pb = Arc::new(Mutex::new(util::init_pb(input_file, None)));
    pb.lock().unwrap().inc();

    for worker_id in 0..num_workers {
        let pb = pb.clone();
        let sender = sender.clone();
        let input_file = input_file.to_string();
        // let input_file = File::open(input_file).unwrap();

        thread::spawn(move || {
            let (vocab, word_count, line_count) =
                learn_vocab_thread(input_file, worker_id, num_workers, pb);
            sender.send((vocab, word_count, line_count)).unwrap(); // thread finished
        });
    }

    // collect all words and phrases
    let mut word_count: u64 = 0;
    let mut line_count: u64 = 0;
    let mut vocab: FnvHashMap<String, (u32, bool)> = FnvHashMap::default();
    for _i in 0..num_workers {
        let (i_vocab, i_word_count, i_line_count) = receiver.recv().unwrap();
        for (i_word, (i_count, i_is_phrase)) in i_vocab {
            let (count, _is_phrase) = vocab.entry(i_word.to_string()).or_insert((0, i_is_phrase));
            *count += i_count;
        }
        word_count += i_word_count;
        line_count += i_line_count;
    }

    let mut vocab_word_count: usize = 0;
    let mut vocab_phrase_count: usize = 0;
    let vocab: FnvHashMap<String, (u32, bool)> = vocab
        .into_iter()
        .filter(|(_entry, (count, is_phrase))| *count >= min_freq as u32 || *is_phrase)
        .map(|(entry, (count, is_phrase))| {
            if is_phrase {
                vocab_phrase_count += 1
            } else {
                vocab_word_count += 1
            };

            (entry, (count, is_phrase))
        })
        .collect();
    println!("Vocabulary size {:.1}", vocab.len());
    println!("Words in vocabulary: {}", vocab_word_count);
    println!("Phrases in vocabulary: {}", vocab_phrase_count);

    (vocab, word_count, line_count)
}

pub fn phrase_thread(
    vocab: Arc<FnvHashMap<String, (u32, bool)>>,
    min_freq: f64,
    word_count: f64,
    threshold: f64,
    worker_id: usize,
    num_workers: usize,
    input_file: String,
    mut out_phrases_file: BufWriter<File>,
    pb: Arc<Mutex<ProgressBar<Stdout>>>,
) -> (FnvHashSet<String>, FnvHashSet<String>) {
    // write text corpus with learned phrases
    let mut curr_line: u64 = 0;
    let mut keep_phrases: FnvHashSet<String> = FnvHashSet::default();
    let mut disc_phrases: FnvHashSet<String> = FnvHashSet::default();
    let mut phrase_lines = String::from("");
    let mut rand = Kiss64Random::new(Some(worker_id as u64));

    let reader = util::reader(&input_file);
    for line in reader.lines() {
        curr_line += 1;
        if curr_line % num_workers as u64 != worker_id as u64 {
            continue;
        }

        let mut prev_word: String = "".to_string();

        let line = line.unwrap();
        let mut phrase_line = String::with_capacity(line.len() + 1);
        let words: Vec<String> = line.split(" ").map(|w| w.trim().to_string()).collect();

        for word in words {
            let (phrase, out_of_vocab) = if prev_word == "" {
                ("".to_string(), true)
            } else {
                let mut phrase = String::with_capacity(word.len() + prev_word.len() + 1);
                phrase.push_str(&prev_word);
                phrase.push_str("_");
                phrase.push_str(&word);

                if !vocab.contains_key(&phrase) {
                    (phrase, true)
                } else if disc_phrases.contains(&phrase) {
                    (phrase, true)
                } else if keep_phrases.contains(&phrase) {
                    (phrase, false)
                } else if phrase.matches("_").count() > 3 {
                    disc_phrases.insert(phrase.clone());
                    (phrase, true)
                } else {
                    // mutual information score
                    let c1 = vocab.get(&word).unwrap_or(&(1, false)).0 as f64;
                    let c2 = vocab.get(&prev_word).unwrap_or(&(1, false)).0 as f64;
                    let c12 = vocab.get(&phrase).unwrap().0 as f64;

                    let out_of_vocab = (c12 - min_freq) / c1 / c2 * word_count <= threshold;
                    if !out_of_vocab {
                        keep_phrases.insert(phrase.clone());
                    } else {
                        disc_phrases.insert(phrase.clone());
                    }

                    (phrase, out_of_vocab)
                }
            };

            if prev_word == "" {
                prev_word = word.clone();
            } else {
                if !out_of_vocab && rand.flip() {
                    // keep phrases with 50% prob
                    phrase_line.push_str(&format!("{} ", phrase));
                    // set the prev word to empty string
                    prev_word = "".to_string();
                } else {
                    phrase_line.push_str(&format!("{} ", prev_word));
                    // update the prev word
                    prev_word = word.clone();
                }
            };
        }

        if prev_word != "" {
            phrase_line.push_str(&format!("{}\n", prev_word));
        } else {
            phrase_line.push_str("\n");
        };

        phrase_lines = format!("{}{}", phrase_lines, phrase_line);
        if phrase_lines.len() > memstat::MB {
            out_phrases_file
                .write_all(phrase_lines.as_bytes())
                .expect(&format!("Worker {}: Failed to write data", worker_id));
            phrase_lines = String::from("");
            pb.lock().unwrap().inc();
        }
    }

    if phrase_lines.len() > 0 {
        out_phrases_file
            .write_all(phrase_lines.as_bytes())
            .expect(&format!("Worker {}: Failed to write data", worker_id));
        pb.lock().unwrap().inc();
    }

    (keep_phrases, disc_phrases)
}

pub fn train(
    input_file: &str,
    out_vocab_path: &Path,
    out_phrases_path: &Path,
    min_freq: u32,
    threshold: f64,
    num_workers: usize,
) -> Result<(), Error> {
    util::rm_when_exist(out_phrases_path.to_str().unwrap());

    let (vocab, word_count, line_count): (FnvHashMap<String, (u32, bool)>, u64, u64) =
        learn_vocab(input_file, min_freq, num_workers);
    let vocab = Arc::new(vocab);
    println!("Total line count: {}", line_count);
    println!("Total word count: {}", word_count);

    let (sender, receiver): (
        Sender<(FnvHashSet<String>, FnvHashSet<String>)>,
        Receiver<(FnvHashSet<String>, FnvHashSet<String>)>,
    ) = mpsc::channel();

    let pb = Arc::new(Mutex::new(util::init_pb(input_file, None)));
    pb.lock().unwrap().inc();

    for worker_id in 0..num_workers {
        let pb = pb.clone();
        let vocab = vocab.clone();
        let sender = sender.clone();
        let input_file = input_file.to_string();
        let out_phrases_file = OpenOptions::new()
            .create(true)
            .append(true) // will be written by multiple threads
            .open(out_phrases_path)
            .unwrap();
        let out_phrases_file = BufWriter::new(out_phrases_file);

        thread::spawn(move || {
            let phrases = phrase_thread(
                vocab,
                min_freq as f64,
                word_count as f64,
                threshold,
                worker_id,
                num_workers,
                input_file,
                out_phrases_file,
                pb,
            );
            sender.send(phrases).unwrap(); // thread finished
        });
    }

    // collect all removed phrases
    let mut keep_phrases: FnvHashSet<String> = FnvHashSet::default();
    let mut disc_phrases: FnvHashSet<String> = FnvHashSet::default();
    for _i in 0..num_workers {
        let (i_keep_phrases, i_disc_phrases) = receiver.recv().unwrap();
        for phrase in i_keep_phrases {
            keep_phrases.insert(phrase);
        }
        for phrase in i_disc_phrases {
            disc_phrases.insert(phrase);
        }
    }

    // write vocab with kept phrases
    let mut out_vocab_file = File::create(out_vocab_path)?;
    let mut vocab_vec: Vec<(String, u32)> = vocab
        .iter()
        .filter_map(|(w, (c, is_phrase))| {
            if *is_phrase && !keep_phrases.contains(w) {
                None
            } else {
                Some((w.to_string(), *c))
            }
        })
        .collect();
    vocab_vec.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap_or(Ordering::Less).reverse());
    println!(
        "Saving vocab to: {:?}, size: {}, words: {}, kept {}, discarded {} phrases",
        out_vocab_path.as_os_str(),
        vocab_vec.len(),
        vocab_vec.len() - keep_phrases.len(),
        keep_phrases.len(),
        disc_phrases.len(),
    );
    for (word, count) in vocab_vec.into_iter() {
        let buffer = format!("{} {}\n", word, count);
        out_vocab_file.write_all(buffer.as_bytes())?;
    }

    Ok(())
}
