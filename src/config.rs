use std::fs;
use std::io::prelude::*;
use std::path;
use toml;

#[derive(Deserialize, Debug, Clone)]
pub struct Shuffle {
    pub save_file: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Cooccur {
    pub shards: usize,
    pub window_size: usize,
    pub symmetric: usize,
    pub dist_weight: usize,
    pub subsampling: usize,
    pub save_file: String,
    pub max_memory: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct NgramsCooccur {
    pub shards: usize,
    pub window_size: usize,
    pub symmetric: usize,
    pub dist_weight: usize,
    pub subsampling: usize,
    pub save_file: String,
    pub max_memory: String,
}

/// xmax and alpha are weighting function parameters,
/// not extremely sensitive to corpus, though may need
/// adjustment for very small or very large corpora
#[derive(Deserialize, Debug, Clone)]
pub struct Model {
    pub xmax: f32,
    pub alpha: f32,
    pub lambda: f32,
    pub ngrams: usize,
    pub max_iter: usize,
    pub vector_size: usize,
    pub save_file: String,
    pub save_mode: u8,
    pub verbose: u8,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Vocab {
    pub min_freq: usize,
    pub skip_threshold: f32,
    pub vocab_file: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Phrases {
    pub save_file: String,
    pub threshold: f64,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Ngrams {
    pub minn: usize,
    pub maxn: usize,
    pub word_weight: f32,
    pub save_file: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct NegativeSampling {
    pub trials: usize,
    pub min_freq: f64,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    pub vocab: Vocab,
    pub phrases: Phrases,
    pub ngrams: Ngrams,
    pub cooccur: Cooccur,
    pub ngrams_cooccur: NgramsCooccur,
    pub model: Model,
    pub negative_sampling: NegativeSampling,
    pub shuffle: Shuffle,
    pub workers: usize,
}

impl Config {
    pub fn parse<P: AsRef<path::Path>>(path: P) -> Config {
        let mut config_str = String::new();

        let mut file = fs::File::open(path)
            .unwrap_or_else(|err| panic!("Error while opening config: [{}]", err));

        file.read_to_string(&mut config_str)
            .unwrap_or_else(|err| panic!("Error while reading config: [{}]", err));

        let config = toml::from_str(&config_str)
            .unwrap_or_else(|err| panic!("Error while parsing config: [{}]", err));

        config
    }
}

impl Default for Config {
    fn default() -> Self {
        Config::parse("config.toml")
    }
}
