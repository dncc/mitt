extern crate blas;
extern crate byteorder;
extern crate clap;
extern crate flate2;
extern crate fnv;
extern crate fst;
extern crate lapack;
extern crate libc;
extern crate memmap;
extern crate openblas_src;
extern crate pbr;
extern crate rand;
extern crate serde;
extern crate toml;

#[macro_use]
extern crate serde_derive;
extern crate serde_json;

use dict::Dict;
use std::convert::TryInto;
use std::sync::Arc;

use clap::{App, Arg, SubCommand};
use std::path::Path;

pub mod config;
use config::Config;

pub mod cooccur;
pub mod dict;
pub mod memstat;
pub mod model;
pub mod ngrams;
pub mod phrases;
pub mod random;
pub mod shuffle;
pub mod util;
pub mod vocab;

extern crate jemallocator;

// set jemallocator::Jemalloc as the global allocator
#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

#[allow(dead_code)]
fn main() {
    let matches = App::new("mitt: \n")
        .arg(
            Arg::with_name("config")
                .help("Path to the config file.")
                .long("config")
                .takes_value(true),
        )
        .subcommand(
            // ./target/release/mitt vocab input.txt
            SubCommand::with_name("vocab")
                .about("Builds vocabulary")
                .arg(
                    Arg::with_name("input")
                        .help("Input file")
                        .long("input")
                        .short("i")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("min-freq")
                        .help("Words with freq. greater or equal to min-freq will be in the vocab")
                        .long("min-freq")
                        .short("f")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("vocab_file")
                        .help("Vocab output file")
                        .long("vocab-file")
                        .short("v")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("workers")
                        .help("Number of workers")
                        .long("workers")
                        .short("p")
                        .required(false)
                        .takes_value(true),
                ),
        ) // end vocab
        .subcommand(
            // ./target/release/mitt vocab input.txt
            SubCommand::with_name("phrases")
                .about("Build vocabulary with phrases")
                .arg(
                    Arg::with_name("input")
                        .help("Input file")
                        .long("input")
                        .short("i")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("min-freq")
                        .help("Words with freq. greater or equal to min-freq will be in the vocab")
                        .long("min-freq")
                        .short("f")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("out-vocab")
                        .help("Output vocab file")
                        .long("out-vocab")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("out-phrases")
                        .help("Output file with phrases")
                        .long("out-phrases")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("workers")
                        .help("Number of workers")
                        .long("workers")
                        .short("w")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("threshold")
                        .help("Threshold for forming the phrases (higher's less); default 100")
                        .long("threshold")
                        .short("t")
                        .required(false)
                        .takes_value(true),
                ),
        ) // end phrases
        .subcommand(
            // ./target/release/mitt cooccur -v vocab.txt -i input.txt -w 8
            SubCommand::with_name("cooccur")
                .about("Creates table of word co-occurrences")
                .arg(
                    Arg::with_name("input")
                        .help("Input file")
                        .long("input")
                        .short("i")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("vocab_file")
                        .help("Vocabulary file")
                        .long("vocab")
                        .short("v")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("out")
                        .help("File output")
                        .long("out")
                        .short("o")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("window")
                        .help("Word window size")
                        .long("window")
                        .short("w")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("workers")
                        .help("Number of concurrent workers")
                        .long("workers")
                        .short("p")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("shards")
                        .help("Number of shards to split work into")
                        .long("shards")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("max_memory")
                        .help("Maximum amount of memory to use")
                        .long("max-memory")
                        .short("m")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("symmetric")
                        .help("Use symmetric context (exchange roles of word1 and word2)")
                        .long("symmetric")
                        .short("s")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("dist_weight")
                        .help("Use distance weights to count word co-occurrence")
                        .long("dist-weight")
                        .short("d")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("skip-treshold")
                        .help("Probability threshold for word subsampling")
                        .long("skip-threshold")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("word_subsampling")
                        .help("Use word subsampling to omit frequent words")
                        .long("word_subsampling")
                        .required(false)
                        .takes_value(true),
                ),
        ) // end cooccur
        .subcommand(
            SubCommand::with_name("ngrams-cooccur")
                .about("Create table of ngrams co-occurrences")
                .arg(
                    Arg::with_name("input")
                        .help("Input file")
                        .long("input")
                        .short("i")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("vocab_file")
                        .help("Vocabulary file")
                        .long("vocab")
                        .short("v")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("out")
                        .help("File output")
                        .long("out")
                        .short("o")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("window")
                        .help("Word window size")
                        .long("window")
                        .short("w")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("workers")
                        .help("Number of concurrent workers")
                        .long("workers")
                        .short("p")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("shards")
                        .help("Number of shards to split work into")
                        .long("shards")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("max_memory")
                        .help("Maximum amount of memory to use")
                        .long("max-memory")
                        .short("m")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("symmetric")
                        .help("Use symmetric context (exchange roles of word1 and word2)")
                        .long("symmetric")
                        .short("s")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("word_dist_weight")
                        .help("Use distance weights to count word co-occurrence")
                        .long("word-dist-weight")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("minn")
                        .help("Minimum character ngram length")
                        .long("minn")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("maxn")
                        .help("Maximum character ngram length")
                        .long("maxn")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("ngram_dist_weight")
                        .help("Use distance weights to count word co-occurrence")
                        .long("subword-dist-weight")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("word_subsampling")
                        .help("Use word subsampling to omit frequent words")
                        .long("word-subsampling")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("skip-treshold")
                        .help("Probability threshold for word and ngram subsampling")
                        .long("skip-threshold")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("ngram_subsampling")
                        .help("Use sub-word subsampling to omit frequent sub-words")
                        .long("subword-subsampling")
                        .required(false)
                        .takes_value(true),
                ),
        ) // end ngrams-cooccur
        .subcommand(
            // ./target/release/mitt merge -v vocab.txt -i input.txt -w 8
            SubCommand::with_name("merge")
                .about("Merges co-occurrence shards to one binary file")
                .arg(
                    Arg::with_name("input_dir")
                        .help("Input dir")
                        .long("input-dir")
                        .short("i")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("vocab_file")
                        .help("Vocabulary file")
                        .long("vocab")
                        .short("v")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("out")
                        .help("File output")
                        .long("out")
                        .short("o")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("workers")
                        .help("Number of concurrent workers")
                        .long("workers")
                        .short("p")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("shards")
                        .help("Number of shards to split work into")
                        .long("shards")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("max_memory")
                        .help("Max. amount of memory to use")
                        .long("max-memory")
                        .short("m")
                        .required(false)
                        .takes_value(true),
                ),
        ) // merge
        .subcommand(
            // ./target/release/mitt shuffle -i cooccurrence.bin
            SubCommand::with_name("shuffle")
                .about("Shuffles table of word or/and ngram co-occurrences")
                .arg(
                    Arg::with_name("input")
                        .help("Input file")
                        .long("input")
                        .short("i")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("out")
                        .help("File output")
                        .long("out")
                        .short("o")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("workers")
                        .help("Number of concurrent workers")
                        .long("workers")
                        .short("p")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("max_memory")
                        .help("Max. amount of memory to use")
                        .long("max-memory")
                        .short("m")
                        .required(false)
                        .takes_value(true),
                ),
        ) // shuffle
        .subcommand(
            SubCommand::with_name("train")
                .about("Trains model")
                .arg(
                    Arg::with_name("workers")
                        .help("Number of concurrent workers")
                        .long("workers")
                        .short("p")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("vector_size")
                        .help("Number of vector dimensions")
                        .long("vector-size")
                        .short("d")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("input")
                        .help("Input file")
                        .long("input")
                        .short("i")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("out")
                        .help("File output")
                        .long("out")
                        .short("o")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("vocab_file")
                        .help("Vocabulary file")
                        .long("vocab")
                        .short("v")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("verbose")
                        .help("Print debugging info")
                        .long("verbose")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("ngrams")
                        .help("Train vectors for char ngrams")
                        .long("ngrams")
                        .short("n")
                        .required(false)
                        .takes_value(false),
                )
                .arg(
                    Arg::with_name("max_iter")
                        .help("Number of iterations to train the model")
                        .long("max-iter")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("xmax")
                        .help("Glove weighting function param, scaling value")
                        .long("xmax")
                        .short("x")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("alpha")
                        .help("Glove weighting function param, power value")
                        .long("alpha")
                        .short("a")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("lambda")
                        .help("Regularization rate")
                        .long("lambda")
                        .short("l")
                        .required(false)
                        .takes_value(true),
                ),
        ) // end train
        .subcommand(
            // ./mitt combine -i subword.vecs.txt -o combined.vecs.txt
            SubCommand::with_name("combine")
                .about("Combines word and character ngram vectors")
                .arg(
                    Arg::with_name("input")
                        .help("File with word and subword vectors")
                        .long("input")
                        .short("i")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("vocab_file")
                        .help("Vocabulary file")
                        .long("vocab")
                        .short("v")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("vector_size")
                        .help("Number of vector dimensions")
                        .long("vector-size")
                        .short("d")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("word_weight")
                        .help("Weight to combine full word with its n-grams")
                        .long("save-mode")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("minn")
                        .help("Minimum character ngram length")
                        .long("minn")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("maxn")
                        .help("Maximum character ngram length")
                        .long("maxn")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("output_format")
                        .help("Output format: 0 - binary, 1 - text")
                        .long("output-format")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("save_mode")
                        .help("Save mode: 0 - binary, 1 - text")
                        .long("save-mode")
                        .required(false)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("out_dir")
                        .help("Directory to save combined vectors in")
                        .long("out-dir")
                        .required(true)
                        .takes_value(true),
                ),
        ) // end combine
        .get_matches();

    println!(
        "Config path: {}",
        matches.value_of("config").unwrap_or("Not provided")
    );

    let config = {
        if let Some(config_path) = matches.value_of("config") {
            Config::parse(config_path)
        } else {
            Config::default()
        }
    };

    if matches.is_present("vocab") {
        if let Some(submatches) = matches.subcommand_matches("vocab") {
            let input_file = submatches.value_of("input").unwrap();

            let min_freq = submatches
                .value_of("min-freq")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.vocab.min_freq);

            let vocab_file = submatches
                .value_of("vocab_file")
                .unwrap_or(&config.vocab.vocab_file);

            let num_workers = submatches
                .value_of("workers")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.workers);

            println!("Building vocabulary...");
            println!("Input file: {}", input_file);
            println!("Vocab file: {}", vocab_file);
            println!("Min. frequency: {}", min_freq);
            println!("Number of workers: {}", num_workers);

            match vocab::build(&input_file, min_freq, &vocab_file, num_workers) {
                Ok(_) => println!("OK"),
                Err(err) => println!("{:?}", err),
            }
        }
    } // end vocab

    if matches.is_present("phrases") {
        if let Some(submatches) = matches.subcommand_matches("phrases") {
            let input_file = submatches.value_of("input").unwrap();

            let min_freq = submatches
                .value_of("min-freq")
                .unwrap_or("")
                .parse::<u32>()
                .unwrap_or(config.vocab.min_freq.try_into().unwrap());

            let out_vocab = submatches
                .value_of("out-vocab")
                .unwrap_or(&config.vocab.vocab_file);
            let out_vocab_path = &Path::new(&out_vocab);

            let out_phrases = submatches
                .value_of("out-phrases")
                .unwrap_or(&config.phrases.save_file);
            let out_phrases_path = &Path::new(&out_phrases);

            let threshold = submatches
                .value_of("threshold")
                .unwrap_or("")
                .parse::<f64>()
                .unwrap_or(config.phrases.threshold);

            let num_workers = submatches
                .value_of("workers")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.workers);

            println!("Learning phrases...");
            println!("Input file: {}", input_file);
            println!("Output file with vocab: {}", out_vocab);
            println!("Output file with phrases: {}", out_phrases);
            println!("Min. word frequency: {}", min_freq);
            println!("Threshold: {}", threshold);
            println!("Number of workers: {}", num_workers);

            match phrases::train(
                &input_file,
                out_vocab_path,
                out_phrases_path,
                min_freq,
                threshold,
                num_workers,
            ) {
                Ok(_) => println!("OK"),
                Err(err) => println!("{:?}", err),
            }
        }
    } // end phrases

    if matches.is_present("combine") {
        if let Some(submatches) = matches.subcommand_matches("combine") {
            let vec_file_path = submatches.value_of("input").unwrap();
            let vocab_file_path = submatches.value_of("vocab_file").unwrap();

            let vector_size = submatches
                .value_of("vector_size")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.model.vector_size);

            let minn = submatches
                .value_of("minn")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.ngrams.minn);

            let maxn = submatches
                .value_of("maxn")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.ngrams.maxn);

            let word_weight = submatches
                .value_of("word_weight")
                .unwrap_or("")
                .parse::<f32>()
                .unwrap_or(config.ngrams.word_weight);

            let save_mode = submatches
                .value_of("save_mode")
                .unwrap_or("")
                .parse::<u8>()
                .unwrap_or(config.model.save_mode);
            let save_mode = model::SaveMode::from(save_mode);

            let output_format = submatches
                .value_of("output_format")
                .unwrap_or("")
                .parse::<u8>()
                .unwrap_or(1);
            // 0 - binary, 1 - txt
            let output_format = ngrams::vocab::OutputFormat::from(output_format);

            let out_dir = submatches.value_of("out_dir").unwrap();

            println!("Combining word and ngram vectors");
            println!("Input vectors file: {}", vec_file_path);
            println!("Input vocab: {}", vocab_file_path);
            println!("Vector size: {}", vector_size);
            println!("Ngram lengths: {}:{}", minn, maxn);
            println!(
                "Weight to combine full word with its n-grams: {}",
                if word_weight < 0.0 {
                    "SIF".to_string()
                } else {
                    format!("{}", word_weight)
                }
            );
            println!("Output dir: {}", out_dir);
            println!("Save: {}", String::from(save_mode));
            println!("Output format: {}", String::from(output_format));

            let use_ngrams = true;
            let dict = Arc::new(Dict::from_file(
                vocab_file_path,
                use_ngrams,
                None,
                None,
                Some(minn),
                Some(maxn),
            ));
            println!("Number of unique words: {}", dict.nwords);
            println!("Number of unique ngrams: {}", dict.nngrams);
            println!("Total entries: {}", dict.size);
            println!("Number of tokens (w/ freq.): {:?}", dict.ntokens);
            println!("Number of word tokens (w/ freq.): {:?}", dict.nword_tokens);
            println!(
                "Number of ngram tokens (w/ freq.): {:?}",
                dict.nngram_tokens
            );

            match ngrams::vocab::combine(
                dict,
                vec_file_path,
                vector_size,
                word_weight,
                save_mode,
                output_format,
                &out_dir,
            ) {
                Ok(_) => println!("OK"),
                Err(err) => println!("{:?}", err),
            }
        }
    } // end combine

    if matches.is_present("cooccur") {
        if let Some(submatches) = matches.subcommand_matches("cooccur") {
            let input_file_path = submatches.value_of("input").unwrap();
            let vocab_file_path = submatches.value_of("vocab_file").unwrap();

            let out_file_path = submatches
                .value_of("out")
                .unwrap_or(&config.cooccur.save_file);

            let window_size = submatches
                .value_of("window")
                .unwrap_or("w")
                .parse::<usize>()
                .unwrap_or(config.cooccur.window_size);

            let symmetric = submatches
                .value_of("symmetric")
                .unwrap_or("s")
                .parse::<usize>()
                .unwrap_or(config.cooccur.symmetric)
                == 1;

            let dist_weight = submatches
                .value_of("dist_weight")
                .unwrap_or("d")
                .parse::<usize>()
                .unwrap_or(config.cooccur.dist_weight)
                == 1;

            let subsampling = submatches
                .value_of("word_subsampling")
                .unwrap_or("ws")
                .parse::<usize>()
                .unwrap_or(config.cooccur.subsampling)
                == 1;

            let num_workers = submatches
                .value_of("workers")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.workers);

            let num_shards = submatches
                .value_of("shards")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.cooccur.shards);

            let max_memory = submatches
                .value_of("max_memory")
                .unwrap_or(&config.cooccur.max_memory)
                .replace("GB", "")
                .parse::<u64>()
                .unwrap();

            let skip_threshold = submatches
                .value_of("skip-threshold")
                .unwrap_or("")
                .parse::<f32>()
                .unwrap_or(config.vocab.skip_threshold);

            println!("Counting cooccurances...");
            println!("Vocabulary: {}", vocab_file_path);
            println!("Window size: {}", window_size);
            println!(
                "Context: {}",
                if symmetric { "symmetric" } else { "asymmetric" }
            );
            println!(
                "Distance weighting: {}",
                if dist_weight { "ON" } else { "OFF" }
            );
            println!(
                "Word subsampling: {}",
                if subsampling { "ON" } else { "OFF" }
            );
            println!("Subsampling threshold: {:?}", skip_threshold);
            println!("Max. memory: {} GB", max_memory);
            println!("Number of shards: {}", num_shards);
            println!("Number of workers: {}", num_workers);

            let init_ngrams = false;
            let dict = Arc::new(Dict::from_file(
                vocab_file_path,
                init_ngrams,
                None,
                Some(skip_threshold),
                None,
                None,
            ));
            println!("Number of unique words: {}", dict.nwords);
            println!("Number of unique ngrams: {}", dict.nngrams);
            println!("Total entries: {}", dict.size);
            println!("Number of tokens (w/ freq.): {:?}", dict.ntokens);
            println!("Number of word tokens (w/ freq.): {:?}", dict.nword_tokens);
            println!(
                "Number of ngram tokens (w/ freq.): {:?}",
                dict.nngram_tokens
            );

            match cooccur::build(
                input_file_path,
                out_file_path,
                dict,
                window_size,
                symmetric,
                dist_weight,
                false,
                subsampling,
                false,
                num_workers,
                num_shards,
                max_memory,
            ) {
                Ok(_) => println!("OK"),
                Err(err) => println!("{:?}", err),
            }
        }
    } // end cooccur

    if matches.is_present("ngrams-cooccur") {
        if let Some(submatches) = matches.subcommand_matches("ngrams-cooccur") {
            let input_file_path = submatches.value_of("input").unwrap();
            let vocab_file_path = submatches.value_of("vocab_file").unwrap();

            let out_file_path = submatches
                .value_of("out")
                .unwrap_or(&config.ngrams_cooccur.save_file);

            let window_size = submatches
                .value_of("window")
                .unwrap_or("w")
                .parse::<usize>()
                .unwrap_or(config.ngrams_cooccur.window_size);

            let symmetric = submatches
                .value_of("symmetric")
                .unwrap_or("s")
                .parse::<usize>()
                .unwrap_or(config.ngrams_cooccur.symmetric)
                == 1;

            let word_dist_weight = submatches
                .value_of("word_dist_weight")
                .unwrap_or("dw")
                .parse::<usize>()
                .unwrap_or(config.cooccur.dist_weight)
                == 1;

            let ngram_dist_weight = submatches
                .value_of("ngram_dist_weight")
                .unwrap_or("sdw")
                .parse::<usize>()
                .unwrap_or(config.ngrams_cooccur.dist_weight)
                == 1;

            let word_subsampling = submatches
                .value_of("word_subsampling")
                .unwrap_or("ws")
                .parse::<usize>()
                .unwrap_or(config.cooccur.subsampling)
                == 1;

            let ngram_subsampling = submatches
                .value_of("ngram_subsampling")
                .unwrap_or("sws")
                .parse::<usize>()
                .unwrap_or(config.ngrams_cooccur.subsampling)
                == 1;

            let num_workers = submatches
                .value_of("workers")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.workers);

            let num_shards = submatches
                .value_of("shards")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.ngrams_cooccur.shards);

            let max_memory = submatches
                .value_of("max_memory")
                .unwrap_or(&config.ngrams_cooccur.max_memory)
                .replace("GB", "")
                .parse::<u64>()
                .unwrap();

            let skip_threshold = submatches
                .value_of("skip-threshold")
                .unwrap_or("")
                .parse::<f32>()
                .unwrap_or(config.vocab.skip_threshold);

            let minn = submatches
                .value_of("minn")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.ngrams.minn);

            let maxn = submatches
                .value_of("maxn")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.ngrams.maxn);

            println!("Counting ngrams cooccurances...");
            println!("Vocabulary: {}", vocab_file_path);
            println!("Window size: {}", window_size);
            println!(
                "Context: {}",
                if symmetric { "symmetric" } else { "asymmetric" }
            );
            println!(
                "Word distance weighting: {}",
                if word_dist_weight { "ON" } else { "OFF" }
            );
            println!(
                "Ngram distance weighting: {}",
                if ngram_dist_weight { "ON" } else { "OFF" }
            );
            println!(
                "Word subsampling: {}",
                if word_subsampling { "ON" } else { "OFF" }
            );
            println!(
                "Ngram subsampling: {}",
                if ngram_subsampling { "ON" } else { "OFF" }
            );
            println!("Subsampling threshold: {:?}", skip_threshold);
            println!("Ngram lengths: {:?}, {:?}", minn, maxn);
            println!("Number of shards: {}", num_shards);
            println!("Number of workers: {}", num_workers);

            let init_ngrams = true;
            let dict = Arc::new(Dict::from_file(
                vocab_file_path,
                init_ngrams,
                None,
                Some(skip_threshold),
                Some(minn),
                Some(maxn),
            ));
            println!("Number of unique words: {}", dict.nwords);
            println!("Number of unique ngrams: {}", dict.nngrams);
            println!("Total entries: {}", dict.size);
            println!("Number of tokens (w/ freq.): {:?}", dict.ntokens);
            println!("Number of word tokens (w/ freq.): {:?}", dict.nword_tokens);
            println!(
                "Number of ngram tokens (w/ freq.): {:?}",
                dict.nngram_tokens
            );

            match cooccur::build(
                input_file_path,
                out_file_path,
                dict,
                window_size,
                symmetric,
                word_dist_weight,
                ngram_dist_weight,
                word_subsampling,
                ngram_subsampling,
                num_workers,
                num_shards,
                max_memory,
            ) {
                Ok(_) => println!("OK"),
                Err(err) => println!("{:?}", err),
            }
        }
    } // end ngrams-cooccur

    if matches.is_present("merge") {
        if let Some(submatches) = matches.subcommand_matches("merge") {
            let input_dir_path = submatches.value_of("input_dir").unwrap();
            let vocab_file_path = submatches.value_of("vocab_file").unwrap();

            let out_file_path = submatches
                .value_of("out")
                .unwrap_or(&config.cooccur.save_file);

            let num_workers = submatches
                .value_of("workers")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.workers);

            let num_shards = submatches
                .value_of("shards")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.cooccur.shards);

            let max_memory = submatches
                .value_of("max_memory")
                .unwrap_or(&config.cooccur.max_memory)
                .replace("GB", "")
                .parse::<u64>()
                .unwrap();

            println!("Merging co-occurrence shards...");
            println!("Vocabulary: {}", vocab_file_path);
            println!("Max. memory: {} GB", max_memory);
            println!("Number of shards: {}", num_shards);
            println!("Number of workers: {}", num_workers);

            match cooccur::merge_from_dir(
                input_dir_path,
                vocab_file_path,
                out_file_path,
                num_workers,
                num_shards,
                max_memory,
            ) {
                Ok(_) => println!("OK"),
                Err(err) => println!("{:?}", err),
            }
        }
    } // end merge

    if matches.is_present("shuffle") {
        if let Some(submatches) = matches.subcommand_matches("shuffle") {
            let cooccur_file_path = submatches.value_of("input").unwrap();

            let num_workers = submatches
                .value_of("workers")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.workers);

            let out_file_path = submatches
                .value_of("out")
                .unwrap_or(&config.shuffle.save_file);

            let max_memory = submatches
                .value_of("max_memory")
                .unwrap_or(&config.cooccur.max_memory)
                .replace("GB", "")
                .parse::<u64>()
                .unwrap();

            println!("Shuffling cooccurances...");
            println!("Co-occurrence file: {}", cooccur_file_path);
            println!("Max. memory: {} GB", max_memory);
            println!("Number of workers: {}", num_workers);

            match shuffle::run(
                cooccur_file_path.to_string(),
                out_file_path.to_string(),
                num_workers,
                max_memory,
            ) {
                Ok(_) => println!("OK"),
                Err(err) => println!("{:?}", err),
            }
        }
    } // end shuffle

    if matches.is_present("train") {
        if let Some(submatches) = matches.subcommand_matches("train") {
            let input_file_path = submatches.value_of("input").unwrap();
            let vocab_file_path = submatches.value_of("vocab_file").unwrap();
            let out_file_path = submatches
                .value_of("out")
                .unwrap_or(&config.model.save_file);

            let num_workers = submatches
                .value_of("workers")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.workers);

            let xmax = submatches
                .value_of("xmax")
                .unwrap_or("")
                .parse::<f32>()
                .unwrap_or(config.model.xmax);

            let vector_size = submatches
                .value_of("vector_size")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.model.vector_size);

            let max_iter = submatches
                .value_of("max_iter")
                .unwrap_or("")
                .parse::<usize>()
                .unwrap_or(config.model.max_iter);

            let alpha = submatches
                .value_of("alpha")
                .unwrap_or("")
                .parse::<f32>()
                .unwrap_or(config.model.alpha);

            let save_mode = submatches
                .value_of("save_mode")
                .unwrap_or("")
                .parse::<u8>()
                .unwrap_or(config.model.save_mode);
            let save_mode = model::SaveMode::from(save_mode);

            let verbose = submatches
                .value_of("verbose")
                .unwrap_or("")
                .parse::<u8>()
                .unwrap_or(config.model.verbose);
            let verbose_mode = model::VerboseMode::from(verbose);

            let use_ngrams = submatches
                .value_of("ngrams")
                .unwrap_or("n")
                .parse::<usize>()
                .unwrap_or(config.model.ngrams)
                == 1;

            let (minn, maxn) = (
                submatches
                    .value_of("minn")
                    .unwrap_or("")
                    .parse::<usize>()
                    .unwrap_or(config.ngrams.minn),
                submatches
                    .value_of("minn")
                    .unwrap_or("")
                    .parse::<usize>()
                    .unwrap_or(config.ngrams.maxn),
            );

            let lambda = submatches
                .value_of("lambda")
                .unwrap_or("")
                .parse::<f32>()
                .unwrap_or(config.model.lambda);

            println!("Training...");
            println!("Co-occurrences: {:?}", input_file_path);
            println!("Vocabulary: {:?}", vocab_file_path);
            println!(
                "Training for ngrams: {:?}, ngram lengths: {:?}, {:?}",
                if use_ngrams { "YES" } else { "NO" },
                minn,
                maxn
            );

            println!("---");
            println!("Number of workers: {}", num_workers);
            println!("alpha: {}", alpha);
            println!("lambda: {}", lambda);
            println!("x-max: {}", xmax);
            println!("Vector size: {}", vector_size);
            println!("Max iterations: {}", max_iter);
            println!("---");

            let dict = Arc::new(Dict::from_file(
                vocab_file_path,
                use_ngrams,
                None,
                None,
                Some(minn),
                Some(maxn),
            ));
            println!("Number of unique words: {}", dict.nwords);
            println!("Number of unique ngrams: {}", dict.nngrams);
            println!("# of dict. entries: {}", dict.size);
            println!("  # of tokens (w/ freq.): {:?}", dict.ntokens);
            println!("  # of word tokens (w/ freq.): {:?}", dict.nword_tokens);
            println!(
                "Number of ngram tokens (w/ freq.): {:?}",
                dict.nngram_tokens
            );
            println!("---");
            println!("Negative sampling:");
            println!("  Trials: {}", config.negative_sampling.trials);
            println!("  Min. freq.: {}", config.negative_sampling.min_freq);
            println!("---");
            match model::train(
                input_file_path,
                dict,
                xmax,
                vector_size,
                max_iter,
                alpha,
                lambda,
                num_workers,
                out_file_path,
                save_mode,
                verbose_mode,
                config.negative_sampling.trials,
                config.negative_sampling.min_freq,
            ) {
                Ok(_) => println!("OK"),
                Err(err) => println!("{:?}", err),
            }
        }
    } // end train
}
