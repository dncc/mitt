use fnv::FnvHashMap;
use std::borrow::Cow;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use vocab;

pub const SKIP_THRESH: f32 = 0.001;
pub const COLLISIONS_THRESH: u32 = 100;

pub const BOW: &str = "<";
pub const EOW: &str = ">";
pub const EOS: &str = "</s>";
pub const EMPTY_ID: u32 = 0;
pub const EMPTY_STR: &str = "";

pub const MAX_VOCAB_SIZE: usize = 25_000_000;

#[inline]
pub fn min<T: PartialOrd>(a: T, b: T) -> T {
    if a < b {
        a
    } else {
        b
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum EntryType {
    NGRAM,
    WORD,
}

#[derive(Clone, Debug)]
pub struct Entry {
    pub id: u32,
    pub token: String, // word or ngram
    pub count: u64,
    pub ngram_ids: Option<FnvHashMap<usize, Vec<u32>>>,
    pub token_type: EntryType,
}

impl PartialEq for Entry {
    #[inline]
    fn eq(&self, other: &Entry) -> bool {
        self.token == other.token && self.id == other.id
    }
}

impl Entry {
    pub fn new(id: u32, token: String, count: u64, token_type: EntryType) -> Entry {
        let ngram_ids = match token_type {
            EntryType::WORD => Some(FnvHashMap::default()),
            EntryType::NGRAM => None,
        };

        Entry {
            id: id,
            token: token,
            count: count,
            ngram_ids: ngram_ids,
            token_type: token_type,
        }
    }

    pub fn get_empty() -> Entry {
        Entry {
            id: EMPTY_ID,
            token: "".to_string(),
            count: std::u64::MAX,
            ngram_ids: None,
            token_type: EntryType::WORD,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.token == EMPTY_STR
    }

    #[inline]
    pub fn get_word(&self) -> Option<String> {
        if self.token_type != EntryType::WORD {
            return None;
        }
        let token = self.token.clone();

        Some(token[1..token.len() - 1].to_string())
    }

    #[inline]
    pub fn is_uppercase(&self) -> bool {
        if self.token.starts_with(BOW) {
            vocab::is_uppercase(&self.token[1..])
        } else {
            vocab::is_uppercase(&self.token)
        }
    }
}

#[derive(Debug)]
pub struct Dict {
    pub size: usize,
    pub nwords: u32,
    pub nngrams: u32,
    pub ntokens: u64,
    pub nword_tokens: u64,
    pub nngram_tokens: u64,
    entry2id: Vec<u32>, // 100MB
    entries: Vec<Entry>,
    pskip: Vec<f32>,
    pub minn: usize,
    pub maxn: usize,
    pub max_size: usize,
    pub skip_threshold: f32,
}

impl Dict {
    fn new(
        max_size: Option<usize>,
        skip_threshold: Option<f32>,
        minn: Option<usize>,
        maxn: Option<usize>,
    ) -> Self {
        // no word should have an id == 0 as word ids start from 1, so 0s are ok here
        let max_size = max_size.unwrap_or(MAX_VOCAB_SIZE);
        let skip_threshold = skip_threshold.unwrap_or(SKIP_THRESH);
        let entry2id: Vec<u32> = vec![0; max_size]; // FIX: ensure it doesn't grow!
        let minn = minn.unwrap_or(4);
        let maxn = maxn.unwrap_or(4);
        // align word/ngram indexes with word/ngram ids (as the ids start from 1)
        let entries = vec![Entry::get_empty()];
        let size = 1;
        // word probs. for randomly skipping freq. entries
        let pskip = vec![];

        Dict {
            nwords: 0,
            nngrams: 0,
            ntokens: 0,
            nngram_tokens: 0,
            nword_tokens: 0,
            entries: entries,
            size: size,
            entry2id: entry2id,
            pskip: pskip,
            minn: minn,
            maxn: maxn,
            max_size: max_size,
            skip_threshold: skip_threshold,
        }
    }

    #[inline]
    fn hash<'a, S: Into<Cow<'a, str>>>(&self, input: S) -> u32 {
        let mut h: u32 = 2166136261;
        for byte in input.into().bytes() {
            h = h ^ (byte as u32);
            h = h.wrapping_mul(16777619);
        }

        h
    }

    #[inline]
    pub fn find_entry_index(&self, token: &str) -> usize {
        let h = self.hash(token) as usize; // u32 -> usize
        let mut i = h % self.max_size;
        let mut count = 0;
        loop {
            if (self.entry2id[i] != 0 && self.entries[self.entry2id[i] as usize].token == token)
                || (self.entry2id[i] == 0 && i != 0)
            {
                break;
            }
            i = (i + 1) % self.max_size;
            count += 1;
            if count > COLLISIONS_THRESH {
                panic!("Too many collisions, consider increasing index size!");
            }
        }

        i
    }

    #[inline]
    pub fn get_pskip(&self, id: u32) -> f32 {
        self.pskip[id as usize]
    }

    #[inline]
    pub fn get_entry(&self, id: usize) -> &Entry {
        &self.entries[id]
    }

    #[inline]
    pub fn get_entry_by_token(&self, token: &str) -> &Entry {
        // Note: for out-of-dictionary entries, will return an Empty entry (id==index==0)
        &self.entries[self.get_entry_id(token) as usize]
    }

    #[inline]
    pub fn get_entry_id(&self, token: &str) -> u32 {
        self.entry2id[self.find_entry_index(token)]
    }

    #[inline]
    pub fn compute_char_ngrams<'a, S: Into<Cow<'a, str>>>(&self, input: S) -> (Vec<String>, usize) {
        let chars: Vec<char> = input.into().chars().collect();
        let chars_len = chars.len();
        if chars_len <= self.minn {
            return (vec![], chars_len);
        }

        let mut ngrams: Vec<String> = vec![];
        for i in 0..chars_len {
            let mut n: usize = 1;
            let mut ngram = String::with_capacity(self.maxn);
            for j in i..chars_len {
                if n > self.maxn {
                    break;
                }
                ngram.push(chars[j]);
                if n >= self.minn && !(n == 1 && i == 0) {
                    ngrams.push(ngram.clone());
                }
                n += 1;
            }
        }

        (ngrams, chars_len)
    }

    pub fn get_probabilities(&self) -> Vec<f32> {
        let mut probs: Vec<f32> = Vec::with_capacity(self.size);
        for id in 0..self.size {
            probs.push((self.entries[id].count as f64 / self.ntokens as f64) as f32);
        }

        probs
    }

    fn init_pskip_table(&mut self) {
        self.pskip = self
            .get_probabilities()
            .into_iter()
            .map(|p| 1.0 - (SKIP_THRESH / p).sqrt())
            .collect();
    }

    fn init_ngrams(&mut self) {
        for id in 1..self.size {
            if self.entries[id].token == EOS
                || self.entries[id].token_type == EntryType::NGRAM
                || self.entries[id].is_uppercase()
            {
                continue;
            }

            let word_count = self.entries[id].count;
            let word_ngram = self.entries[id].token.clone();
            let (char_ngrams, word_ngram_len) = self.compute_char_ngrams(word_ngram);

            // add ngram entries first to obtain their ids
            let vec_cap = (self.minn..self.maxn + 1).fold(0, |mut s, l| {
                s += if word_ngram_len > l {
                    word_ngram_len - l + 1
                } else {
                    0
                };

                s
            });
            let mut char_ngram_ids = Vec::with_capacity(vec_cap);
            for ngram in char_ngrams.iter() {
                char_ngram_ids.push(self.add_entry(
                    ngram.to_string(),
                    word_count,
                    EntryType::NGRAM,
                ));
            }

            // update word ngram ids
            if let Some(ref mut ngram_ids) = &mut self.entries[id].ngram_ids {
                ngram_ids.clear();
                // add the word's id
                // ngram_ids.push(id as u32);
                // add its ngrams' ids
                for (ngram_id, ngram) in char_ngram_ids.iter().zip(char_ngrams.iter()) {
                    let ref mut len_ngram_ids = *ngram_ids.entry(ngram.len()).or_insert(vec![]);
                    len_ngram_ids.push(*ngram_id);
                }
            }
        }
    }

    #[inline]
    pub fn add_entry(&mut self, token: String, count: u64, token_type: EntryType) -> u32 {
        let token = match token_type {
            EntryType::WORD => format!("{}{}{}", BOW, token, EOW),
            _ => token,
        };

        let ei = self.find_entry_index(&token);
        if self.entry2id[ei] == 0 {
            let entry_id = self.size as u32; // entry ids start from 1
            self.entry2id[ei] = entry_id;
            let entry = Entry::new(entry_id, token, count, token_type);
            self.entries.push(entry);
            self.size += 1;
            match token_type {
                EntryType::WORD => {
                    self.nwords += 1;
                    self.nword_tokens += count;
                }
                EntryType::NGRAM => {
                    self.nngrams += 1;
                    self.nngram_tokens += count;
                }
            };
        } else {
            self.entries[self.entry2id[ei] as usize].count += count;
            match token_type {
                EntryType::WORD => self.nword_tokens += count,
                EntryType::NGRAM => self.nngram_tokens += count,
            };
        }
        self.ntokens += count;

        // TODO should return Result<Error, u32>
        return self.entry2id[ei];
    }

    // read vocabulary file
    pub fn from_file(
        vocab_file_path: &str,
        init_ngrams: bool,
        max_size: Option<usize>,
        skip_threshold: Option<f32>,
        minn: Option<usize>,
        maxn: Option<usize>,
    ) -> Dict {
        let vocab_file = BufReader::new(File::open(vocab_file_path).unwrap());
        let mut dict = Dict::new(max_size, skip_threshold, minn, maxn);
        for line in vocab_file.lines() {
            let (word, count) = vocab::parse_vocab_line(&line.unwrap()).unwrap();
            dict.add_entry(word, count as u64, EntryType::WORD);
        }

        if init_ngrams {
            dict.init_ngrams();
        }
        dict.init_pskip_table();

        dict
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compute_char_ngrams() {
        let dict = Dict::new(Some(10), None, Some(4), Some(4));

        let w = "<a>";
        let e: Vec<&str> = vec![];
        let (ngrams, _word_len) = dict.compute_char_ngrams(w);
        assert_eq!(
            ngrams.iter().map(|ngram| &**ngram).collect::<Vec<&str>>(),
            e
        );

        let w = "<of>";
        let e: Vec<&str> = vec![];
        let (ngrams, _word_len) = dict.compute_char_ngrams(w);
        assert_eq!(
            ngrams.iter().map(|ngram| &**ngram).collect::<Vec<&str>>(),
            e
        );

        let w = "<the>";
        let e = vec!["<the", "the>"];
        let (ngrams, _word_len) = dict.compute_char_ngrams(w);
        assert_eq!(
            ngrams.iter().map(|ngram| &**ngram).collect::<Vec<&str>>(),
            e
        );

        let w = "<anarchism>";
        let e = vec![
            "<ana", "anar", "narc", "arch", "rchi", "chis", "hism", "ism>",
        ];
        let (ngrams, _word_len) = dict.compute_char_ngrams(w);
        assert_eq!(
            ngrams.iter().map(|ngram| &**ngram).collect::<Vec<&str>>(),
            e
        );

        let dict = Dict::new(Some(1), None, Some(3), Some(6));

        let w = "<a>";
        let e: Vec<&str> = vec![];
        let (ngrams, _word_len) = dict.compute_char_ngrams(w);
        assert_eq!(
            ngrams.iter().map(|ngram| &**ngram).collect::<Vec<&str>>(),
            e
        );

        let w = "<of>";
        let e = vec!["<of", "<of>", "of>"];
        let (ngrams, _word_len) = dict.compute_char_ngrams(w);
        assert_eq!(
            ngrams.iter().map(|ngram| &**ngram).collect::<Vec<&str>>(),
            e
        );

        let w = "<the>";
        let e = vec!["<th", "<the", "<the>", "the", "the>", "he>"];
        let (ngrams, _word_len) = dict.compute_char_ngrams(w);
        assert_eq!(
            ngrams.iter().map(|ngram| &**ngram).collect::<Vec<&str>>(),
            e
        );

        let w = "<anarchism>";
        let e = vec![
            "<an", "<ana", "<anar", "<anarc", "ana", "anar", "anarc", "anarch", "nar", "narc",
            "narch", "narchi", "arc", "arch", "archi", "archis", "rch", "rchi", "rchis", "rchism",
            "chi", "chis", "chism", "chism>", "his", "hism", "hism>", "ism", "ism>", "sm>",
        ];
        let (ngrams, _word_len) = dict.compute_char_ngrams(w);
        assert_eq!(
            ngrams.iter().map(|ngram| &**ngram).collect::<Vec<&str>>(),
            e
        );
    }

    #[test]
    fn test_add_words() {
        let words = vec!["a", "of", "the", "anarchism"];
        let max_size: usize = 10;
        let mut dict = Dict::new(Some(max_size), None, Some(4), Some(4));
        for word in words.clone() {
            dict.add_entry(word.to_string(), 1, EntryType::WORD);
        }

        assert_eq!(dict.size, 5); // +1 for a dummy entry to align word ids and indexes
        assert_eq!(dict.ntokens, 4);
        assert_eq!(dict.nwords, 4);
        assert_eq!(dict.minn, 4);
        assert_eq!(dict.maxn, 4);
        assert_eq!(
            dict.entries
                .iter()
                .map(|e| &*e.token)
                .collect::<Vec<&str>>()[1..]
                .to_vec(),
            words
                .iter()
                .map(|w| format!("{}{}{}", BOW, w, EOW))
                .collect::<Vec<String>>()
        );

        assert_eq!(dict.entry2id, vec![0, 1, 3, 0, 0, 0, 0, 0, 2, 4]);

        // test entry2id mapping
        assert_eq!(dict.find_entry_index("<a>"), 1);
        assert_eq!(dict.find_entry_index("<of>"), 8);
        assert_eq!(dict.find_entry_index("<the>"), 2);
        assert_eq!(dict.find_entry_index("<anarchism>"), 9);

        assert_eq!(dict.entry2id[dict.find_entry_index("<a>")], 1);
        assert_eq!(dict.entry2id[dict.find_entry_index("<of>")], 2);
        assert_eq!(dict.entry2id[dict.find_entry_index("<the>")], 3);
        assert_eq!(dict.entry2id[dict.find_entry_index("<anarchism>")], 4);

        assert_eq!(dict.get_entry_id("<a>"), 1);
        assert_eq!(dict.get_entry_id("<of>"), 2);
        assert_eq!(dict.get_entry_id("<the>"), 3);
        assert_eq!(dict.get_entry_id("<anarchism>"), 4);
    }

    #[test]
    fn test_init_ngrams_init_pskip_table() {
        let words = vec!["a", "of", "the", "anarchism"];
        let count = vec![20000, 10000, 30000, 10];
        let max_size = 20;

        let mut dict = Dict::new(Some(max_size), None, Some(4), Some(4));
        for (i, word) in words.clone().iter().enumerate() {
            dict.add_entry(word.to_string(), count[i], EntryType::WORD);
        }

        assert_eq!(dict.minn, 4);
        assert_eq!(dict.maxn, 4);
        assert_eq!(dict.size, 5); // +1 for a dummy entry to align word ids and indexes
        assert_eq!(dict.nwords, 4);
        assert_eq!(dict.nngrams, 0);
        assert_eq!(dict.ntokens, 60010);

        assert_eq!(
            dict.entries
                .iter()
                .map(|e| &*e.token)
                .collect::<Vec<&str>>()[1..]
                .to_vec(),
            words
                .iter()
                .map(|w| format!("{}{}{}", BOW, w, EOW))
                .collect::<Vec<String>>()
        );

        assert_eq!(
            dict.entry2id,
            [0, 1, 3, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4]
        );

        assert_eq!(dict.get_entry_id("<a>"), 1);
        assert_eq!(dict.get_entry_id("<of>"), 2);
        assert_eq!(dict.get_entry_id("<the>"), 3);
        assert_eq!(dict.get_entry_id("<anarchism>"), 4);

        dict.init_ngrams();
        assert_eq!(dict.size, 15);
        assert_eq!(dict.nwords, 4);
        assert_eq!(dict.nngrams, 10);
        assert_eq!(dict.ntokens, 120090);
        assert_eq!(
            dict.entries
                .iter()
                .map(|e| &*e.token)
                .collect::<Vec<&str>>()[1..]
                .to_vec(),
            vec![
                "<a>",
                "<of>",
                "<the>",
                "<anarchism>",
                "<the",
                "the>",
                "<ana",
                "anar",
                "narc",
                "arch",
                "rchi",
                "chis",
                "hism",
                "ism>"
            ]
        );
        assert_eq!(
            dict.entry2id,
            [0, 1, 3, 7, 9, 8, 5, 13, 2, 0, 12, 14, 0, 11, 6, 0, 0, 10, 0, 4]
        );

        assert_eq!(dict.get_entry_id("<a>"), 1);
        assert_eq!(dict.get_entry_id("<of>"), 2);
        assert_eq!(dict.get_entry_id("<the>"), 3);
        assert_eq!(dict.get_entry_id("<the"), 5);
        assert_eq!(dict.get_entry_id("the>"), 6);
        assert_eq!(dict.get_entry_id("<anarchism>"), 4);
        assert_eq!(dict.get_entry_id("<ana"), 7);
        assert_eq!(dict.get_entry_id("anar"), 8);
        assert_eq!(dict.get_entry_id("narc"), 9);
        assert_eq!(dict.get_entry_id("arch"), 10);

        assert_eq!(
            dict.entries[1].ngram_ids.as_ref().unwrap(),
            &vec![].into_iter().collect()
        );

        assert_eq!(
            dict.entries[2].ngram_ids.as_ref().unwrap(),
            &vec![].into_iter().collect()
        );

        assert_eq!(
            dict.entries[3].ngram_ids.as_ref().unwrap(),
            &vec![(4, vec![5, 6])].into_iter().collect()
        );

        assert_eq!(
            dict.entries[4].ngram_ids.as_ref().unwrap(),
            &vec![(4, vec![7, 8, 9, 10, 11, 12, 13, 14])]
                .into_iter()
                .collect()
        );

        dict.init_pskip_table();
        assert_eq!(
            dict.pskip
                .iter()
                .map(|p| (p * 100.0).round() / 100.0)
                .collect::<Vec<f32>>(),
            // 'anarchism' and its n-grams are rare enough to have zero probabilities
            vec![
                1.0, 0.92, 0.89, 0.94, -2.47, 0.94, 0.94, -2.47, -2.47, -2.47, -2.47, -2.47, -2.47,
                -2.47, -2.47
            ]
        );
    }

    #[test]
    fn test_get_entry() {
        let words = vec!["a", "of", "the", "anarchism"];
        let count = vec![20000, 10000, 30000, 10];
        let max_size = 20;

        let mut dict = Dict::new(Some(max_size), None, Some(4), Some(4));
        for (i, word) in words.clone().iter().enumerate() {
            dict.add_entry(word.to_string(), count[i], EntryType::WORD);
        }
        dict.init_ngrams();

        let entry_id = dict.get_entry_id(&format!("{}{}{}", BOW, "oov-word", EOW));
        let entry = dict.get_entry(entry_id as usize);
        assert_eq!(entry_id, 0);
        assert_eq!(entry.token, "".to_string());
        assert_eq!(entry, &Entry::get_empty());

        let entry_id = dict.get_entry_id(&format!("{}{}{}", BOW, "the", EOW));
        let entry = dict.get_entry(entry_id as usize);
        assert_eq!(entry_id, 3);
        assert_eq!(entry.token, "<the>".to_string());
        assert_eq!(
            entry,
            &Entry {
                id: 3,
                token: "<the>".to_string(),
                count: 1,
                ngram_ids: None,
                token_type: EntryType::WORD
            }
        );
    }
}
