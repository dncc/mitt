use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;

use fnv::FnvHashMap;
use std::cmp::{Ordering, PartialOrd};
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, Error};
use std::sync::{Arc, Mutex};

use memstat;
use util;
pub const MAX_STRING_LENGTH: usize = 50;

pub fn parse_vocab_line(line: &str) -> Result<(String, usize), Error> {
    let split: Vec<&str> = line.split(" ").map(|t| t).collect();
    let (word, count) = match split.len() {
        2 => (split[0].to_string(), split[1].parse::<usize>().unwrap()),
        _ => panic!("Failed to parse vocabulary line!"),
    };

    Ok((word, count))
}

// read vocabulary file into vector
pub fn load_words_counts(vocab_file_path: &str) -> Vec<usize> {
    // read and load vocabulary
    let vocab_file = BufReader::new(File::open(vocab_file_path).unwrap());

    vocab_file
        .lines()
        .map(|l| {
            let (_word, count) = parse_vocab_line(&l.unwrap()).unwrap();

            count
        })
        .collect::<Vec<usize>>()
}

// read vocabulary file
pub fn load(vocab_file_path: &str) -> FnvHashMap<String, (usize, usize)> {
    // read and load vocabulary
    let vocab_file = BufReader::new(File::open(vocab_file_path).unwrap());

    vocab_file
        .lines()
        .enumerate()
        .map(|(i, l)| {
            let (word, count) = parse_vocab_line(&l.unwrap()).unwrap();

            (word, (i + 1, count)) // word ids start from 1
        })
        .collect::<FnvHashMap<String, (usize, usize)>>()
}

#[inline]
pub fn to_uppercase(s: &str) -> String {
    let mut c = s.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f.to_uppercase().chain(c).collect(),
    }
}

#[inline]
pub fn to_lowercase(s: &str) -> String {
    let mut c = s.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f.to_lowercase().chain(c).collect(),
    }
}

#[inline]
pub fn is_uppercase(s: &str) -> bool {
    let mut c = s.chars();
    match c.next() {
        None => false,
        Some(f) => f.to_uppercase().to_string() == f.to_string() && !f.is_numeric(),
    }
}

#[inline]
pub fn is_lowercase(s: &str) -> bool {
    let mut c = s.chars();
    match c.next() {
        None => false,
        Some(f) => f.to_lowercase().to_string() == f.to_string() && !f.is_numeric(),
    }
}

pub trait LineParser {
    fn insert(&mut self, line: &str);
    fn get_words(&self) -> &FnvHashMap<String, usize>;
    fn get_begin_words(&self) -> &FnvHashMap<String, usize>;
}

pub struct Words {
    pub words: FnvHashMap<String, usize>,
    pub begin_words: FnvHashMap<String, usize>,
}

impl LineParser for Words {
    fn insert(&mut self, line: &str) {
        let words: Vec<&str> = line.split_whitespace().collect();
        for (i, word) in words.into_iter().enumerate() {
            if i == 0 {
                let count = self.begin_words.entry(word.to_string()).or_insert(0);
                *count += 1;
            }
            let count = self.words.entry(word.to_string()).or_insert(0);
            *count += 1;
        }
    }

    fn get_words(&self) -> &FnvHashMap<String, usize> {
        &self.words
    }

    fn get_begin_words(&self) -> &FnvHashMap<String, usize> {
        &self.begin_words
    }
}

pub fn build(
    input_file: &str,
    min_freq: usize,
    vocab_file: &str,
    num_workers: usize,
) -> Result<(), Error> {
    let (sender, receiver): (
        Sender<Box<dyn LineParser + Send>>,
        Receiver<Box<dyn LineParser + Send>>,
    ) = mpsc::channel();

    let pb = Arc::new(Mutex::new(util::init_pb(input_file, None)));
    pb.lock().unwrap().inc();

    // read vocabulary
    for worker_id in 0..num_workers {
        let pb = pb.clone();
        let sender = sender.clone();
        let input_file = input_file.to_string();

        thread::spawn(move || {
            let mut curr_line: u64 = 0;
            let mut proc_data: usize = 0;
            let mut vocab = Box::new(Words {
                words: FnvHashMap::default(),
                begin_words: FnvHashMap::default(),
            }) as Box<dyn LineParser + Send>;

            let reader = util::reader(&input_file);
            for line in reader.lines() {
                curr_line += 1;
                if curr_line % num_workers as u64 != worker_id as u64 {
                    continue;
                }
                let line = line.unwrap();

                vocab.insert(&line);

                proc_data += line.len();
                if proc_data > memstat::MB {
                    pb.lock().unwrap().inc();
                    proc_data -= memstat::MB;
                }
            }

            sender.send(vocab).unwrap(); // thread finished
        });
    }

    let mut all_words: FnvHashMap<String, usize> = FnvHashMap::default();
    let mut begin_words: FnvHashMap<String, usize> = FnvHashMap::default();
    for _i in 0..num_workers {
        let ith_vocab = receiver.recv().unwrap();
        for (word, count) in ith_vocab.get_words() {
            let total_count = all_words.entry(word.to_string()).or_insert(0);
            *total_count += count;
        }

        for (word, count) in ith_vocab.get_begin_words() {
            let total_count = begin_words.entry(word.to_string()).or_insert(0);
            *total_count += count;
        }
    }

    let mut vocab: FnvHashMap<String, usize> = FnvHashMap::default();
    for (word, count) in all_words.iter() {
        let total_count = vocab.entry(word.to_string()).or_insert(0);
        *total_count += count;
    }

    let mut vocab_vec: Vec<(String, usize)> = vocab
        .into_iter()
        .filter(|(word, count)| {
            *count >= min_freq && word.len() > 0 && word.len() <= MAX_STRING_LENGTH
        })
        .collect::<Vec<(String, usize)>>();
    vocab_vec.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap_or(Ordering::Less).reverse());
    println!("Vocabulary size: {:.1}", vocab_vec.len());

    // write vocabulary
    let mut vocab_file = File::create(vocab_file)?;
    for (word, count) in vocab_vec.into_iter() {
        let buffer = format!("{} {}\n", word, count);
        vocab_file.write_all(buffer.as_bytes())?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_uppercase() {
        assert_eq!(to_lowercase("Camel"), "camel");
        assert_eq!(to_lowercase("CamelCamel"), "camelCamel");

        assert_eq!(is_uppercase("CamelCamel"), true);
        assert_eq!(is_uppercase("camelCamel"), false);
        assert_eq!(is_uppercase("12345567"), false);

        assert_eq!(to_uppercase("camel"), "Camel");
        assert_eq!(to_uppercase("camelCamel"), "CamelCamel");

        assert_eq!(is_lowercase("camelCamel"), true);
        assert_eq!(is_lowercase("CamelCamel"), false);
        assert_eq!(is_lowercase("12345567"), false);
    }
}
