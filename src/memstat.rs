use std::fmt;

pub const KB: usize = 1_024;
pub const MB: usize = self::KB * self::KB;
pub const GB: usize = self::KB * self::MB;

#[derive(Default)]
pub struct Bytes(pub usize);

impl fmt::Display for Bytes {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let bytes = self.0;
        if bytes < KB {
            return write!(f, "{} bytes", bytes);
        }
        if bytes < MB {
            return write!(f, "{}kb", bytes / KB);
        }
        if bytes < GB {
            return write!(f, "{}mb", bytes / MB);
        }

        write!(f, "{}gb", bytes / GB)
    }
}

impl std::ops::AddAssign<usize> for Bytes {
    fn add_assign(&mut self, x: usize) {
        self.0 += x;
    }
}

pub struct MemoryStats {
    pub resident: Bytes,
    pub allocated: Bytes,
}

impl MemoryStats {
    pub fn current() -> MemoryStats {
        jemalloc_ctl::epoch().unwrap();
        MemoryStats {
            resident: Bytes(jemalloc_ctl::stats::resident().unwrap()),
            allocated: Bytes(jemalloc_ctl::stats::allocated().unwrap()),
        }
    }

    #[inline]
    pub fn allocated() -> usize {
        jemalloc_ctl::stats::allocated().unwrap()
    }
}

impl fmt::Display for MemoryStats {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt,
            "{} allocated {} resident",
            self.allocated, self.resident,
        )
    }
}
