use std::time::{SystemTime, UNIX_EPOCH};

/// random number generator
/// http://www0.cs.ucl.ac.uk/staff/d.jones/GoodPracticeRNG.pdf
/// https://de.wikipedia.org/wiki/KISS_(Zufallszahlengenerator)

pub trait Random<T> {
    fn new(seed: Option<T>) -> Self;
    fn rand(&mut self) -> T;
    fn flip(&mut self) -> bool;
    fn index(&mut self, n: T) -> T;
}

// 64 bit KISS. Use this if you have more than about 2^24 data points
#[derive(Debug)]
pub struct Kiss64Random {
    x: u64,
    y: u64,
    z: u64,
    c: u64,
}

impl Random<u64> for Kiss64Random {
    fn new(seed: Option<u64>) -> Kiss64Random {
        let mut seed = seed.unwrap_or(0);
        // seed must be different than 0
        if seed == 0 {
            let now = SystemTime::now();
            seed = now.duration_since(UNIX_EPOCH).unwrap().subsec_nanos() as u64;
        }

        Kiss64Random {
            x: seed,
            y: 362_436_362_436_362_436,
            z: 1_066_149_217_761_810,
            c: 123_456_123_456_123_456,
        }
    }

    fn rand(&mut self) -> u64 {
        // Linear congruence generator
        self.z = self.z.wrapping_mul(6_906_969_069).wrapping_add(1_234_567);

        // Xor shift
        self.y ^= self.y << 13;
        self.y ^= self.y >> 17;
        self.y ^= self.y << 43;

        // Multiply-with-carry
        let t: u64 = (self.x << 58).wrapping_add(self.c);
        self.c = self.x >> 6;
        self.x = self.x.wrapping_add(t);
        self.c = self.c.wrapping_add((self.x < t) as u64);

        self.x.wrapping_add(self.y.wrapping_add(self.z))
    }

    fn flip(&mut self) -> bool {
        // Draw random false or true
        (self.rand() & 1) == 1
    }

    fn index(&mut self, n: u64) -> u64 {
        // Draw random integer between 0 and n-1 where n
        // is at most the number of data points you have
        self.rand() % n
    }
}
