use byteorder::{ByteOrder, LittleEndian};
use std::io::prelude::*;
use std::io::{Error, SeekFrom, Stdout};
use std::sync::{Arc, Mutex};

use std::fs::{self, File, OpenOptions};
use std::io::{BufReader, BufWriter};
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;

use pbr::ProgressBar;

use cooccur::Crec;
use random::{Kiss64Random, Random};

use memstat;
use util;

pub const RAND_ALFANUM_LEN: usize = 24;

#[inline]
fn get_crec_len(file_path: &str) -> usize {
    let metadata = std::fs::metadata(file_path).unwrap();

    (metadata.len() / Crec::get_size() as u64) as usize
}

fn read_crecs(
    cooccurs: &mut Vec<Crec>,
    cooccurs_file: &mut BufReader<File>,
    addr: u64,
    len: usize,
) -> u64 {
    let mut bytes_read: u64 = 0;
    cooccurs_file.seek(SeekFrom::Start(addr)).unwrap();

    let mut buf = vec![0u8; Crec::get_size()];
    let word_size = Crec::get_word_size();

    let mut handle;
    for _ in 0..len {
        handle = cooccurs_file.take(Crec::get_size() as u64);
        bytes_read += handle.read(&mut buf).unwrap_or(0) as u64;

        cooccurs.push(Crec {
            w1: LittleEndian::read_u32(&buf[0..word_size]),
            w2: LittleEndian::read_u32(&buf[word_size..2 * word_size]),
            val: LittleEndian::read_f32(&buf[2 * word_size..]),
        });
    }

    assert!(
        bytes_read / Crec::get_size() as u64 == len as u64,
        "Read {:?} co-occurrences, expected {:?}",
        bytes_read / Crec::get_size() as u64,
        len
    );

    bytes_read as u64
}

fn write_all(cooccurs: &Vec<Crec>, out_file: &mut BufWriter<File>) -> Result<u64, Error> {
    let sum = cooccurs.into_iter().fold(0, |mut sum: u64, r| {
        sum += Crec::write(r, out_file) as u64;
        sum
    });

    out_file
        .flush()
        .expect(&format!("Flush failed, file: {:?}", &out_file));

    Ok(sum)
}

fn merge_shuffled(
    file_indexes_chunks: Vec<Vec<usize>>,
    shuf_files: Vec<String>,
    out_file_name: &str,
    pb: Arc<Mutex<ProgressBar<Stdout>>>,
) -> Result<u64, Error> {
    util::rm_when_exist(out_file_name);
    let (sender, receiver): (Sender<u64>, Receiver<u64>) = mpsc::channel();

    let mut rec_count: u64 = 0;
    for file_indexes_chunk in file_indexes_chunks {
        let chunk_len = file_indexes_chunk.len();

        for i in file_indexes_chunk.into_iter() {
            let mut writer = BufWriter::new(
                OpenOptions::new()
                    .create(true)
                    .append(true)
                    .open(out_file_name)
                    .unwrap(),
            );

            let pb = pb.clone();
            let sender = sender.clone();
            let shuf_files = shuf_files.clone();
            let shuf_file = File::open(&shuf_files[i]).unwrap();

            thread::spawn(move || {
                let shuf_crec_len = get_crec_len(&shuf_files[i]);
                let mut cooccurs: Vec<Crec> = Vec::with_capacity(shuf_crec_len);
                let mut reader =
                    BufReader::with_capacity(Crec::get_size() * memstat::MB, shuf_file);

                let bytes_read = read_crecs(&mut cooccurs, &mut reader, 0, shuf_crec_len);
                let bytes_written = self::write_all(&cooccurs, &mut writer).unwrap();
                assert!(
                    bytes_written == bytes_read,
                    "Write error: bytes read {:?}, bytes_written {:?}, file: {:?}",
                    bytes_read,
                    bytes_written,
                    &shuf_files[i],
                );

                pb.lock().unwrap().inc();
                sender.send(shuf_crec_len as u64).unwrap(); // thread finished
            });
        }

        for _ in 0..chunk_len {
            rec_count += receiver.recv().unwrap();
        }
    }

    Ok(rec_count)
}

pub fn run(
    cooccurs_file_path: String,
    out_file: String,
    num_workers: usize,
    max_memory: u64,
) -> Result<(), Error> {
    let (sender, receiver): (Sender<(usize, String)>, Receiver<(usize, String)>) = mpsc::channel();

    let crec_len = get_crec_len(&cooccurs_file_path);
    let cooccur_file_size = crec_len * Crec::get_size();
    let num_batches = util::min(
        4,
        util::max(
            1,
            (cooccur_file_size as f32 / (max_memory / 2) as f32).ceil() as usize,
        ),
    );
    let num_jobs = num_batches * num_workers;
    let chunk_size = (crec_len as f64 / num_jobs as f64).ceil() as usize;

    let mut record_count: u64 = 0;
    let mut tmp_file_names: Vec<String> = Vec::with_capacity(num_jobs);

    println!("Shuffling {:?} cooccur records...", crec_len);
    println!("Number of batches: {:?}", num_batches);
    println!("Number of shuffle jobs: {:?}", num_jobs);
    println!("Number of records per job: {:?}", chunk_size);

    let pb = Arc::new(Mutex::new(ProgressBar::new(2 * num_jobs as u64 + 1)));
    pb.lock().unwrap().inc();

    for batch_id in 0..num_batches {
        for worker_id in 0..num_workers {
            let pb = pb.clone();
            let sender = sender.clone();
            let crec_len = crec_len.clone();
            let chunk_size = chunk_size.clone();
            let cooccurs_file_path = cooccurs_file_path.clone();

            let job_id = batch_id * num_workers + worker_id;
            let cooccurs_file = File::open(&cooccurs_file_path).unwrap();

            thread::spawn(move || {
                let mut rand = Kiss64Random::new(Some(worker_id as u64));
                let mut reader =
                    BufReader::with_capacity(Crec::get_size() * memstat::MB, cooccurs_file);

                let addr = job_id as u64 * chunk_size as u64 * Crec::get_size() as u64;
                let len = util::min((job_id + 1) * chunk_size, crec_len) - (job_id * chunk_size);
                let mut cooccurs: Vec<Crec> = Vec::with_capacity(len);

                let bytes_read = read_crecs(&mut cooccurs, &mut reader, addr, len);

                util::shuffle(&mut cooccurs, &mut rand);

                // write to tmp file
                let tmp_file_name = util::tmp_file_path(
                    &format!("tmp_shuffle_{}_", job_id),
                    &".bin",
                    RAND_ALFANUM_LEN,
                );
                let mut writer = BufWriter::new(
                    OpenOptions::new()
                        .create(true)
                        .append(true)
                        .open(&tmp_file_name)
                        .unwrap(),
                );

                let bytes_written = self::write_all(&cooccurs, &mut writer).unwrap();
                assert!(
                    bytes_written == bytes_read,
                    "Write error: bytes read {:?}, bytes_written {:?}",
                    bytes_read,
                    bytes_written,
                );

                pb.lock().unwrap().inc();
                sender.send((cooccurs.len(), tmp_file_name)).unwrap(); // thread finished
            });
        }

        // wait for the batch to finish
        for _ in 0..num_workers {
            let (rec_count, tmp_file_name) = receiver.recv().unwrap();
            record_count += rec_count as u64;
            tmp_file_names.push(tmp_file_name);
        }
    }

    println!("Shuffled {:.1} records", record_count);

    // check records count
    let crec_len = get_crec_len(&cooccurs_file_path);
    assert!(
        record_count as usize == crec_len,
        "Mismatch: shuffled {:?}, expected {:?}",
        record_count,
        crec_len,
    );

    // merge shuffled
    println!("Merging {:.1} shuffled records", record_count);
    let mut file_indexes: Vec<usize> = (0..num_jobs).into_iter().collect();
    util::shuffle(&mut file_indexes, &mut Kiss64Random::new(None));
    let file_indexes_chunks: Vec<Vec<usize>> = file_indexes
        .chunks(num_batches)
        .map(|c| c.to_vec())
        .collect();

    match merge_shuffled(file_indexes_chunks, tmp_file_names.clone(), &out_file, pb) {
        Ok(count) => {
            println!("Merged {:.1} records to {:?}", count, &out_file);
            // remove temp shuffle files
            for tmp_file_name in tmp_file_names.into_iter() {
                fs::remove_file(tmp_file_name)?;
            }
        }
        Err(err) => return Err(err),
    }

    Ok(())
}
