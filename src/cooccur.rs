use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::sync::{Arc, Mutex, RwLock};
use std::thread;

use byteorder::{ByteOrder, LittleEndian, WriteBytesExt};
use fnv::FnvHashMap;
use fst::{raw, MapBuilder, Streamer};
use pbr::ProgressBar;
use std::cmp::{Ordering, PartialOrd};
use std::fs::{read_dir, File, OpenOptions};
use std::io::prelude::*;
use std::io::BufWriter;
use std::io::{Error, SeekFrom};
use std::mem::size_of;
use std::path::Path;

use memstat;
use random::{Kiss64Random, Random};

use dict::{Dict, Entry, BOW, EOW}; //, EMPTY_ID
use util;
use vocab;

pub const RAND_ALFANUM_LEN: usize = 24;

/// co-occurrence record
#[repr(C, packed)]
#[derive(Clone, Copy, Debug)]
pub struct Crec {
    pub w1: u32,
    pub w2: u32,
    pub val: f32,
}

impl Crec {
    #[inline]
    pub fn get_size() -> usize {
        size_of::<Crec>()
    }

    #[inline]
    pub fn get_word_size() -> usize {
        size_of::<u32>()
    }

    #[inline]
    pub fn write(crec: &Crec, writer: &mut BufWriter<File>) -> u64 {
        let mut buf = Vec::with_capacity(Crec::get_size());
        buf.write_u32::<LittleEndian>(crec.w1).unwrap();
        buf.write_u32::<LittleEndian>(crec.w2).unwrap();
        buf.write_f32::<LittleEndian>(crec.val).unwrap();
        writer
            .write_all(buf.as_slice())
            .expect("Unable to write data");

        buf.len() as u64
    }
}

#[derive(Debug)]
pub struct Shard {
    pub path: String,
    pub writer: BufWriter<File>,
    pub zip_num: usize,
    pub zip_len: u64,
}

impl Shard {
    pub fn new(job_id: usize, id: usize) -> Self {
        let path = util::tmp_file_path(
            &format!("tmp_{}_{}_", job_id, id),
            &".bin",
            RAND_ALFANUM_LEN,
        );

        let writer = BufWriter::new(
            OpenOptions::new()
                .create(true)
                .append(true)
                .open(&path)
                .unwrap(),
        );

        Shard {
            path: path,
            writer: writer,
            zip_num: 0,
            zip_len: 0,
        }
    }

    #[inline]
    pub fn zip(&mut self) {
        let shard_len = Shard::len(&self.path);
        if shard_len > 2_u64.pow(25) + self.zip_len {
            println!(
                "Compressing shard: {:?}, {:?} elements...",
                &self.path, shard_len
            );

            let mut zip: FnvHashMap<u64, f32> = FnvHashMap::default();
            let mut file = File::open(&self.path).unwrap();
            read_crecs(&mut zip, &mut file, shard_len as u64).unwrap();

            // recreate file
            util::rm_when_exist(&self.path);
            self.writer = BufWriter::new(
                OpenOptions::new()
                    .create(true)
                    .append(true)
                    .open(&self.path)
                    .unwrap(),
            );

            // write compressed data
            self.write_all(&mut ShardData { data: zip }, None);
            self.zip_num += 1;
            self.zip_len = Shard::len(&self.path);
            println!(
                "Compressed shard: {:?}, zip: {:?}, len: {:?}, compressed {:?}%...",
                &self.path,
                self.zip_num,
                self.zip_len,
                (100.0 * (1.0 - self.zip_len as f32 / shard_len as f32)).round() as u32
            );
        }
    }

    #[inline]
    pub fn write_all(&mut self, shard_data: &mut ShardData, with_flush: Option<bool>) {
        let with_flush = with_flush.unwrap_or(false);

        let mut buf: Vec<u8> = Vec::with_capacity(shard_data.size());
        for (id, val) in shard_data.data.drain() {
            buf.write_u64::<LittleEndian>(id).unwrap();
            buf.write_f32::<LittleEndian>(val).unwrap();
        }

        self.writer
            .write_all(buf.as_slice())
            .expect(&format!("Write all failed, shard: {:?}", &self.path));

        if with_flush {
            self.writer
                .flush()
                .expect(&format!("Flush failed, shard: {:?}", &self.path));
        }
    }

    #[inline]
    fn len(path: &str) -> u64 {
        let metadata = std::fs::metadata(path).unwrap();

        metadata.len() / (size_of::<u64>() + size_of::<f32>()) as u64
    }
}

#[derive(Clone, Debug)]
pub struct ShardData {
    pub data: FnvHashMap<u64, f32>,
}

impl ShardData {
    pub fn new() -> Self {
        ShardData {
            data: FnvHashMap::default(),
        }
    }

    #[inline]
    pub fn size(&self) -> usize {
        self.data.len() * (size_of::<u64>() + size_of::<f32>())
    }
}

#[inline]
pub fn lookup(word_index: usize, dict_size: usize) -> usize {
    1 + word_index * dict_size
}

// returns co-occurrence array index
#[inline]
pub fn lookup_inv(lookup_idx: usize, dict_size: usize) -> (usize, usize) {
    // assumes that word-ids start from 1
    (lookup_idx / dict_size + 1, lookup_idx % dict_size + 1)
}

// returns co-occurrence array index
#[inline]
pub fn lookup_idx(word_id1: usize, word_id2: usize, dict_size: usize) -> usize {
    // assumes word ids start from 1
    let lookup_idx = (lookup(word_id1 - 1, dict_size) + word_id2) - 2;

    if lookup_inv(lookup_idx, dict_size) != (word_id1, word_id2) {
        panic!(
            "lookup index: {} inverted to: {:?}, expected: ({}, {})",
            lookup_idx,
            lookup_inv(lookup_idx, dict_size),
            word_id1,
            word_id2
        );
    }

    lookup_idx
}

// TODO cooccurs to Vec<Crec> and write them all at once to the file
#[inline]
pub fn write_crecs(
    cooccurs: &mut FnvHashMap<u64, f32>,
    writer: &mut BufWriter<File>,
    dict_size: usize,
) -> u64 {
    let mut cooc_rec: u64 = 0;

    for (i, v) in cooccurs {
        let (w1, w2) = lookup_inv(*i as usize, dict_size);

        let mut buf = Vec::with_capacity(Crec::get_size());
        buf.write_u32::<LittleEndian>(w1 as u32).unwrap();
        buf.write_u32::<LittleEndian>(w2 as u32).unwrap();
        buf.write_f32::<LittleEndian>(*v).unwrap();
        writer
            .write_all(buf.as_slice())
            .expect("Unable to write data");

        cooc_rec += buf.len() as u64;
    }

    cooc_rec / Crec::get_size() as u64
}

pub fn read_crecs(
    cooccurs: &mut FnvHashMap<u64, f32>,
    cooccurs_file: &mut File,
    len: u64,
) -> Result<(), Error> {
    let mut bytes_read: u64 = 0;
    let cooc_id_size = size_of::<u64>();
    let rec_size = cooc_id_size + size_of::<f32>();
    let mut buf = vec![0u8; rec_size];
    let mut handle;
    cooccurs_file.seek(SeekFrom::Start(0)).unwrap();
    for _ in 0..len {
        handle = cooccurs_file.take(rec_size as u64);
        bytes_read += handle.read(&mut buf).unwrap_or(0) as u64;
        let cindex = LittleEndian::read_u64(&buf[0..cooc_id_size]);
        let cvalue = LittleEndian::read_f32(&buf[cooc_id_size..]);
        let val = cooccurs.entry(cindex).or_insert(0.0);
        *val += cvalue;
    }

    assert!(
        bytes_read / rec_size as u64 == len,
        "Read {:?} co-occurrences, expected {:?}",
        bytes_read / rec_size as u64,
        len
    );

    Ok(())
}

pub fn get_num_merge_workers(
    jobs_shards: &FnvHashMap<usize, Vec<String>>,
    max_memory: u64,
    num_workers: usize,
) -> usize {
    let num_jobs = jobs_shards.len();
    let mut shards_size: u64 = 0;
    for (_, shards) in jobs_shards {
        for shard in shards.iter() {
            shards_size += std::fs::metadata(shard).unwrap().len();
        }
    }

    let mem_per_worker = shards_size / num_workers as u64;
    println!(
        "Memory per worker: {}",
        memstat::Bytes(mem_per_worker as usize)
    );

    util::min(
        util::max(
            1,
            ((max_memory * memstat::GB as u64) / mem_per_worker) as usize,
        ),
        num_jobs,
    )
}

pub fn merge(
    job_ids_chunks: Vec<Vec<usize>>,
    jobs_shards: &FnvHashMap<usize, Vec<String>>,
    out_file_name: &str,
    dict_size: usize,
    num_jobs: usize,
    num_shards: usize,
) -> Result<u64, Error> {
    let mut total_rec_count: u64 = 0;

    let (sender, receiver): (Sender<(u64, Vec<String>)>, Receiver<(u64, Vec<String>)>) =
        mpsc::channel();

    let pb = Arc::new(Mutex::new(ProgressBar::new(
        (num_shards * num_jobs) as u64 + 1,
    )));
    pb.lock().unwrap().inc();

    util::rm_when_exist(out_file_name);
    // Parallelize. Co-occurrences are distributed by their id to a shard,
    // so that all co-occurrences with an identical id end up in the same
    // shard (shard with the same id), across different jobs.
    let mut map_filenames = vec![];
    for job_ids_chunk in job_ids_chunks {
        let chunk_len = job_ids_chunk.len();

        for job_id in job_ids_chunk {
            let file = OpenOptions::new()
                .create(true)
                .append(true)
                .open(out_file_name)
                .unwrap();
            let mut writer = BufWriter::new(file);

            let pb = pb.clone();
            let sender = sender.clone();
            let jobs_shards = jobs_shards.clone();
            let out_file_name = out_file_name.to_string().clone();

            // create a map
            let out_map_names: Vec<String> = (0..num_shards)
                .into_iter()
                .map(|i| util::tmp_file_path(&format!("map_{}_{}", job_id, i), &".fst", 0))
                .collect();

            thread::spawn(move || {
                let mut rec_count: u64 = 0;

                for i in 0..num_shards {
                    let mut cooc_len: u64;
                    let mut cooccurs: FnvHashMap<u64, f32> = FnvHashMap::default();
                    let mut file = File::open(&jobs_shards[&job_id][i]).unwrap();
                    cooc_len = Shard::len(&jobs_shards[&job_id][i]);
                    read_crecs(&mut cooccurs, &mut file, cooc_len).unwrap();
                    cooc_len = cooccurs.len() as u64;

                    let write_count = write_crecs(&mut cooccurs, &mut writer, dict_size);
                    assert!(
                        write_count == cooc_len,
                        "Co-occurs written {:?}, expected {:?}, file: {:?}",
                        write_count,
                        cooc_len,
                        &out_file_name,
                    );

                    rec_count += write_count;
                    util::rm_when_exist(&out_map_names[i]);
                    let wrt = BufWriter::new(File::create(&out_map_names[i]).unwrap());
                    let mut build = MapBuilder::new(wrt).unwrap();

                    // sort cooccurs
                    // let mut cooccurs: Vec<([u8; 8], u64)> = cooccurs
                    let mut cooccurs: Vec<(String, u64)> = cooccurs
                        .into_iter()
                        .map(|(id, val)| {
                            // let bytes: [u8; 8] = unsafe { std::mem::transmute(id.to_le()) };
                            (id.to_string(), (val * 100.0).round() as u64)
                        })
                        .collect();
                    cooccurs.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap_or(Ordering::Equal));
                    for (id, val) in cooccurs.into_iter() {
                        build.insert(id, val).unwrap();
                    }
                    build.finish().unwrap();
                    pb.lock().unwrap().inc();
                }

                // remove shards
                for i in 0..num_shards {
                    util::rm_when_exist(&jobs_shards[&job_id][i]);
                }

                sender.send((rec_count, out_map_names)).unwrap(); // thread finished
            });
        }

        for _ in 0..chunk_len {
            let (rec_count, out_map_names) = receiver.recv().unwrap();
            total_rec_count += rec_count;
            for out_map_name in out_map_names {
                map_filenames.push(out_map_name);
            }
        }
    }

    let mut maps = vec![];
    for f in map_filenames.iter() {
        let fst = raw::Fst::from_path(f).unwrap();
        maps.push(fst);
    }
    let mut union = maps.iter().collect::<raw::OpBuilder>().union();
    let wrt = BufWriter::new(File::create(format!("{}.{}", out_file_name, &"fst")).unwrap());
    let mut builder = raw::Builder::new(wrt).unwrap();

    let mut count: u64 = 0;
    let mut pb = ProgressBar::new(total_rec_count / 1_000_000 + 1);
    while let Some((k, v)) = union.next() {
        // v = [IndexValue{index:0, value: 1}, IndexValue{index:1, value: 0}, ... ]
        // the v's length should be always 1 due to how sharding is done
        let index_value = v.to_vec()[0];
        builder.insert(k, index_value.value).unwrap();

        count += 1;
        if count % 1_000_000 == 0 {
            pb.inc();
        }
    }
    builder.finish().unwrap();
    pb.finish();

    assert!(
        count == total_rec_count,
        "Map len: {} and number of records {} don't match",
        count,
        total_rec_count
    );

    // cleanup tmp
    for f in map_filenames {
        util::rm_when_exist(&f);
    }

    Ok(total_rec_count)
}

pub fn read_jobs_shards_dir(input_dir: &str) -> FnvHashMap<usize, Vec<String>> {
    let shards_path = &Path::new(input_dir);
    let shard_paths = if shards_path.is_dir() {
        let mut shards: Vec<_> = read_dir(shards_path)
            .unwrap()
            .map(|s| s.unwrap().path())
            .filter(|s| s.to_str().unwrap().ends_with(".bin"))
            .collect();
        shards.sort();
        shards
    } else {
        // nothing to merge
        vec![]
    };

    let mut jobs_shards: FnvHashMap<usize, Vec<String>> = FnvHashMap::default();
    for shard_path in shard_paths {
        let shard = shard_path.to_str().unwrap();
        let file_name_parts: Vec<&str> = shard.split("/").last().unwrap().split("_").collect();
        let job_id = file_name_parts[1].parse::<usize>().unwrap();

        let shards = jobs_shards.entry(job_id).or_insert(vec![]);
        shards.push(shard.to_string());
    }

    jobs_shards
}

pub fn merge_from_dir(
    input_dir: &str,
    vocab_file: &str,
    out_file: &str,
    num_workers: usize,
    num_shards: usize,
    max_memory: u64,
) -> Result<(), Error> {
    let vocab = vocab::load(vocab_file);
    let dict_size = vocab.len();
    println!("Vocabulary size: {:?}", vocab.len());

    // divide work in N batches of parallel jobs (x <num_workers>)
    let num_batches = util::max((num_shards as f32 / num_workers as f32).ceil() as usize, 1);
    // how many jobs in total, used to distribute input lines
    let num_jobs = num_batches * num_workers;

    let jobs_shards = read_jobs_shards_dir(input_dir);

    // merge
    println!("Merging shards...");
    let mut shard_ids: Vec<usize> = (0..num_shards).into_iter().collect();
    util::shuffle(&mut shard_ids, &mut Kiss64Random::new(None));

    let num_merge_workers = get_num_merge_workers(&jobs_shards, max_memory, num_workers);
    println!("Number of parallel merge jobs: {:?}", num_merge_workers);
    let shard_ids_chunks: Vec<Vec<usize>> = shard_ids
        .chunks(num_merge_workers)
        .map(|c| c.to_vec())
        .collect();

    match merge(
        shard_ids_chunks,
        &jobs_shards,
        &out_file,
        dict_size,
        num_jobs,
        num_shards,
    ) {
        Ok(count) => {
            println!("Merged {:.1} records to {:?}", count, &out_file);

            Ok(())
        }
        Err(err) => return Err(err),
    }
}

#[inline]
fn get_cooccurs<R: Random<u64>>(
    entries: &Vec<&Entry>,
    window: usize,
    dict_size: usize,
    symmetric: bool,
    word_dist_weight: bool,
    ngram_dist_weight: bool,
    word_subsampling: bool,
    ngram_subsampling: bool,
    dict: &Dict,
    rand: &mut R,
) -> Vec<(usize, f32)> {
    // cooc. vec capacity
    let ng_count: usize = entries.iter().fold(0, |mut s, e| {
        if let Some(ngram_ids) = &e.ngram_ids {
            s += ngram_ids.len();
        }

        s
    });
    let cap = if ng_count > 1 {
        ng_count * (ng_count - 1)
    } else {
        ng_count
    };
    let mut cooccurs: Vec<(usize, f32)> = Vec::with_capacity(cap);

    // collect word co-occurrences
    let mut j = 0;
    let mut history: Vec<u32> = vec![0; window];
    for (_, entry2) in entries.iter().enumerate() {
        if word_subsampling
            && util::skip_rnd(dict.get_pskip(entry2.id) as f64, dict.ntokens as f64, rand)
        {
            // history[j % window] = EMPTY_ID;
            // j += 1;
            continue;
        }

        let begin = util::max(j as i32 - window as i32, 0);
        for k in (begin..j as i32).rev() {
            let entry1_id = unsafe { *history.get_unchecked(k as usize % window) };
            // if entry1_id == EMPTY_ID {
            // continue;
            // }

            // TODO pre-compute
            let word_dist = if !word_dist_weight {
                1.0
            } else {
                util::dist_weight(window, j, k)
            };

            cooccurs.push((
                lookup_idx(entry1_id as usize, entry2.id as usize, dict_size),
                word_dist,
            ));
            if symmetric {
                cooccurs.push((
                    lookup_idx(entry2.id as usize, entry1_id as usize, dict_size),
                    word_dist,
                ));
            }
        }

        history[j % window] = entry2.id;
        j += 1;
    }

    if dict.nngrams == 0 {
        return cooccurs;
    }

    // collect ngram co-occurrences
    let mut j = 0;
    let ngram_window = window
        * (dict.minn..=dict.maxn).fold(0, |mut s, l| {
            s += l;
            s
        });

    let ngram_ids: Vec<u32> = entries.iter().fold(vec![], |mut v, e| {
        if let Some(ng_ids) = &e.ngram_ids {
            // collect all ngram ids (w/o word ids) by len
            for ngram_len in dict.minn..=dict.maxn {
                v.extend(ng_ids.get(&ngram_len).unwrap_or(&vec![]));
            }
        }

        v
    });

    let mut history: Vec<u32> = vec![0; ngram_window];
    for (_, ng_id2) in ngram_ids.iter().enumerate() {
        if ngram_subsampling
            && util::skip_rnd(dict.get_pskip(*ng_id2) as f64, dict.ntokens as f64, rand)
        {
            // history[j % window] = EMPTY_ID;
            // j += 1;
            continue;
        }

        let begin = util::max(j as i32 - ngram_window as i32, 0);
        for k in (begin..j as i32).rev() {
            let ng_id1 = unsafe { *history.get_unchecked(k as usize % ngram_window) };

            // TODO pre-compute
            // if ng_id1 == EMPTY_ID {
            // continue;
            // }

            let ngram_dist = if !ngram_dist_weight {
                1.0
            } else {
                util::dist_weight(ngram_window, j, k)
            };

            // ngram co-occurrences
            cooccurs.push((
                lookup_idx(ng_id1 as usize, *ng_id2 as usize, dict_size),
                ngram_dist,
            ));

            if symmetric {
                cooccurs.push((
                    lookup_idx(*ng_id2 as usize, ng_id1 as usize, dict_size),
                    ngram_dist,
                ));
            }
        }

        history[j % ngram_window] = *ng_id2;
        j += 1;
    }

    return cooccurs;
}

pub fn build(
    input_file: &str,
    out_file: &str,
    dict: Arc<Dict>,
    window_size: usize,
    symmetric: bool,
    word_dist_weight: bool,
    ngram_dist_weight: bool,
    word_subsampling: bool,
    ngram_subsampling: bool,
    num_workers: usize,
    num_shards: usize,
    max_memory: u64,
) -> Result<(), Error> {
    let (sender, receiver): (
        Sender<(u64, usize, Vec<String>)>,
        Receiver<(u64, usize, Vec<String>)>,
    ) = mpsc::channel();

    let pb = Arc::new(Mutex::new(util::init_pb(input_file, None)));
    pb.lock().unwrap().inc();

    // divide work in N batches of parallel jobs (x <num_workers>)
    let num_batches = util::max((num_shards as f32 / num_workers as f32).ceil() as usize, 1);
    // how many jobs in total, used to distribute input lines
    let num_jobs = num_batches * num_workers;

    // Create num_jobs * num_shards shards. This is necessary for large text corpuses (>500GB).
    // Without distributing to shards, co-occurrences become to big to store, even on disk.
    let mut shards: Vec<Arc<RwLock<Shard>>> = Vec::with_capacity(num_jobs * num_shards);
    for job_id in 0..num_jobs {
        for id in 0..num_shards {
            shards.push(Arc::new(RwLock::new(Shard::new(job_id, id))));
        }
    }

    let mem_per_shard = util::max(
        60 * memstat::KB, // TODO add an input argument
        memstat::GB / (num_workers * num_shards * num_jobs),
    );

    let mut record_count: u64 = 0;
    let mut jobs_shards: FnvHashMap<usize, Vec<String>> = FnvHashMap::default();
    for batch_id in 0..num_batches {
        for worker_id in 0..num_workers {
            let pb = pb.clone();
            let dict = dict.clone();
            let sender = sender.clone();
            let shards = shards.clone();
            let input_file = input_file.to_string();
            let job_id = batch_id * num_workers + worker_id;

            // Each worker has a dedicated shard data buffer for each individual shard,
            // so each worker ends up writing data to every shards separately
            let mut shard_data: Vec<ShardData> = Vec::with_capacity(num_jobs * num_shards);
            for _ in 0..(num_jobs * num_shards) {
                shard_data.push(ShardData::new());
            }

            thread::spawn(move || {
                let mut proc_data: usize = 0;
                let mut line_count: u64 = 0;
                let mut rec_count: u64 = 0;
                let mut rand = Kiss64Random::new(Some(worker_id as u64));
                let reader = util::reader(&input_file);

                for line in reader.lines() {
                    line_count += 1;
                    if line_count % num_jobs as u64 != job_id as u64 {
                        continue;
                    };

                    let line = line.unwrap();
                    let entries = line
                        .split(" ")
                        .filter_map(|w| {
                            if w.len() > vocab::MAX_STRING_LENGTH {
                                return None;
                            };

                            let word = format!("{}{}{}", BOW, w, EOW);
                            let entry = dict.get_entry_by_token(&word);
                            if entry.is_empty() {
                                return None;
                            }

                            return Some(entry);
                        })
                        .collect::<Vec<&Entry>>();

                    let cooccurs = get_cooccurs(
                        &entries,
                        util::min(window_size, entries.len()),
                        dict.size,
                        symmetric,
                        word_dist_weight,
                        ngram_dist_weight,
                        word_subsampling,
                        ngram_subsampling,
                        &dict,
                        &mut rand,
                    );

                    rec_count += cooccurs.len() as u64;
                    for cooc in cooccurs {
                        // [i % (num_jobs * num_shards)] will place uniquely co-occurrence ids;
                        // each one in the same shard data buffer, across different workers.
                        // This is ok as long as all workers write eventually their shard data
                        // buffers to the same corresponding shard.
                        let i = cooc.0;
                        let val = shard_data[i % (num_jobs * num_shards)]
                            .data
                            .entry(i as u64) // store by global id
                            .or_insert(0.0);
                        *val += cooc.1;
                    }

                    proc_data += line.len();
                    if proc_data > memstat::MB {
                        pb.lock().unwrap().inc();
                        proc_data -= memstat::MB;
                    }

                    for i in 0..(num_jobs * num_shards) {
                        if shard_data[i].size() > mem_per_shard as usize {
                            let mut shard_i = shards[i].write().unwrap();
                            shard_i.write_all(&mut shard_data[i], None);
                            shard_i.zip();
                        }
                    }
                }

                // drain the rest of the shard_data
                let mut shard_paths: Vec<String> = Vec::with_capacity(num_shards);
                let shard_range = job_id * num_shards..(job_id + 1) * num_shards;
                for (i, shard) in shards.into_iter().enumerate() {
                    let with_flush = Some(true);
                    shard
                        .write()
                        .unwrap()
                        .write_all(&mut shard_data[i], with_flush);
                    if shard_range.contains(&i) {
                        shard_paths.push(shard.read().unwrap().path.to_string());
                    }
                }

                if proc_data > 0 {
                    pb.lock().unwrap().inc();
                }

                sender.send((rec_count, job_id, shard_paths)).unwrap(); // thread finished
            });
        }

        // wait for the batch to finish
        for _ in 0..num_workers {
            let (rec_count, job_id, shards) = receiver.recv().unwrap();
            record_count += rec_count;
            // job_id  => Vec<shard_file_path>
            jobs_shards.insert(job_id, shards);
        }
    }
    println!("Processed {:.1} records", record_count);

    // merge
    println!("Merging shards...");
    let mut job_ids: Vec<usize> = (0..num_jobs).into_iter().collect();
    util::shuffle(&mut job_ids, &mut Kiss64Random::new(None));

    let num_merge_workers = get_num_merge_workers(&jobs_shards, max_memory, num_workers);
    println!("Number of parallel merge jobs: {:?}", num_merge_workers);
    let job_ids_chunks: Vec<Vec<usize>> = job_ids
        .chunks(num_merge_workers)
        .map(|c| c.to_vec())
        .collect();

    match merge(
        job_ids_chunks,
        &jobs_shards,
        &out_file,
        dict.size,
        num_jobs,
        num_shards,
    ) {
        Ok(count) => {
            println!("Merged {:.1} records to {:?}", count, &out_file);

            Ok(())
        }
        Err(err) => return Err(err),
    }
}
