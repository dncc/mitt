/*

 Combines sub-word embeddings in a word embedding by the SIF¹ (Smooth Inverse Frequency) method.

 It's a 2 step procedure:

    1. Linearly combine word vectors with SIF weights:

            A / (A + p)

        where p is a ratio (word_freqᵢ / sum_freq) and
        A is a smoothing constant (e.g. 0.003).

    2. Substract the first principal component from vectors
       obtained in the step 1.

 [1] "A Simple but Tough-to-Beat Baseline for Sentence Embeddings"
      https://openreview.net/pdf?id=SyK00v5xx

*/
use dict::{Dict, Entry};
use fnv::FnvHashMap;
use ngrams::vocab::WordVec;
use std::sync::Arc;
use util;

const _RMPC: usize = 1; // number of principal components to remove in SIF weighting scheme
const SIF_PARAM: f64 = 1e-3; // the parameter in the SIF weighting scheme, usually in the range [1e-5, 1e-3], the bigger the data set the smaller should be the constant

#[inline]
pub fn combine(
    entry: &Entry,
    dict: &Arc<Dict>,
    word_weight: f32,
    vector_size: usize,
    ngram_vecs: &FnvHashMap<String, WordVec>,
    probs: &Vec<f32>,
) -> WordVec {
    // let ngrams: FnvHashSet<String> = compute_ngrams(word, subword_len).into_iter().collect();
    let mut sif_norm: f64 = 0.0;
    // let ngram_ids = entry.ngram_ids.as_ref().unwrap();
    let mut sifs_vecs: Vec<(f64, &WordVec)> =
        entry
            .ngram_ids
            .as_ref()
            .unwrap()
            .iter()
            .fold(vec![], |mut v, (_ng_len, ng_ids)| {
                for ng_id in ng_ids {
                    let ngram = &dict.get_entry(*ng_id as usize).token;
                    if !ngram_vecs.contains_key(ngram) {
                        continue;
                    }

                    let sif = SIF_PARAM / (SIF_PARAM + probs[*ng_id as usize] as f64);
                    sif_norm += sif;

                    v.push((sif, ngram_vecs.get(ngram).unwrap()));
                }

                v
            });

    if sif_norm == 0.0 {
        sif_norm = 1.0;
    }

    // add word weight
    if let Some(word_vec) = ngram_vecs.get(&entry.token) {
        let mut sif_word_weight = if word_weight < 0.0 {
            SIF_PARAM / (SIF_PARAM + probs[entry.id as usize] as f64)
        } else {
            word_weight as f64
        };
        sif_word_weight *= sif_norm;

        sifs_vecs.push((sif_word_weight, word_vec))
    } else {
        println!("Word vec for '{}' not found.", &entry.token);
    }

    let subword_vec = sifs_vecs
        .iter()
        .fold(vec![0.0; vector_size], |mut sv, (sif, vec)| {
            // normalize sif weights for n-grams on the fly
            unsafe { blas::daxpy(vector_size as i32, *sif / sif_norm, &vec.0, 1, &mut sv, 1) }

            sv
        });

    // TODO raise an Exception if it's a zero vector
    WordVec(subword_vec)
}

#[inline]
pub fn remove_pc(mut mat: &mut Vec<f64>, nrows: usize, ncols: usize) -> WordVec {
    let m = nrows as i32;
    let n = ncols as i32;
    let minmn = util::min(m, n);

    // when JOBZ == 'S'
    let lda = m;
    let ldu = m;
    let ldvt = minmn;

    let u_cols = minmn;
    let vt_rows = minmn;

    let mut s: Vec<f64> = vec![0.0; minmn as usize];

    // 'u_cols' is the number of rows in the output matrix 'u'.
    // As 'u' is a 1D array, first m-elements are the 1st row,
    // second m-elements the 2nd row etc.
    let mut u: Vec<f64> = vec![0.0; (u_cols * m) as usize];

    // 'vt_rows' is the number of columns in the output matrix
    // 'vt'. As 'vt' is transposed, the 1st column is the 1st PC,
    // the 2nd column the 2nd PC etc.
    let mut vt: Vec<f64> = vec![0.0; (n * vt_rows) as usize];

    let lwork = util::max(4 * minmn * minmn + util::max(m, n) + 9 * minmn, 1);
    let mut work = vec![0.0; lwork as usize];
    let mut iwork: Vec<i32> = vec![0; 8 * minmn as usize];
    let mut info = 0;

    // SVD
    unsafe {
        // Fortran implementation of Lapack uses column major ordering so we have to transpose
        // and linearize the input matrix. It is also possible to swap rows and columns (m=cols,
        // n=rows) and thus avoid transpose, but this would place the 1st pc in the first row of
        // the matrix 'u'. Also, `dgsedd` destroys the original matrix, so we are about to make
        // a copy of it anyway. A more efficient and scalable solution would be randomized SVD:
        // https://bit.ly/2KInurD

        let mut a: Vec<f64> = Vec::with_capacity(ncols * nrows);
        for i in 0..nrows {
            for j in 0..ncols {
                *a.get_unchecked_mut(i + j * nrows) = *mat.get_unchecked_mut(i * ncols + j);
            }
        }

        lapack::dgesdd(
            b'S',
            m,
            n,
            &mut a,
            lda,
            &mut s,
            &mut u,
            ldu,
            &mut vt,
            ldvt,
            &mut work,
            lwork as i32,
            &mut iwork,
            &mut info,
        );
    }

    // take the first PC (RMPC = 1), the first column of the vt matrix (n x vt_rows)
    let pc = vt
        .into_iter()
        .step_by(vt_rows as usize)
        .map(|e| -1.0 * e) // flip sign
        .collect::<Vec<f64>>();

    let alpha: f64 = 1.0;
    let incx: i32 = 1;
    let beta: f64 = 1.0;
    let mut y: Vec<f64> = vec![0.0; nrows];
    let incy: i32 = 1;

    // X = X - X.dot(pc.transpose()) * pc;
    unsafe {
        let mut mat_prim: Vec<f64> = vec![0.0; nrows * ncols];
        // y = X.dot(pc.transpose());  ([M x N] . [N x 1] = [M x 1])
        // column major ordering: transpose A
        blas::dgemv(
            b'T',
            ncols as i32,
            nrows as i32,
            alpha,
            &mat,
            ncols as i32,
            &pc,
            incx,
            beta,
            &mut y,
            incy,
        );

        // X' = y * pc; ([M x 1] * [1 x N] = [M x N])
        for i in 0..nrows {
            for j in 0..ncols {
                *mat_prim.get_unchecked_mut(i * ncols + j) =
                    *y.get_unchecked(i) * *pc.get_unchecked(j);
            }
        }

        // X = X - X'; ([M x N] - [M x N] = [M x N])
        blas::daxpy((nrows * ncols) as i32, -1f64, &mat_prim, 1, &mut mat, 1);

        return WordVec(pc);
    }
}
