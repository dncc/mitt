use byteorder::{LittleEndian, WriteBytesExt};
use fnv::FnvHashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, Error};
use std::mem::size_of;
use std::sync::Arc;

use dict::Dict;
use model::SaveMode;
use ngrams::sif;
use vocab;

pub static BOW: &str = "<";
pub static EOW: &str = ">";

use std::num::ParseFloatError;

#[derive(Debug)]
pub enum ParseVecError {
    ParseFloat(ParseFloatError),
    MissmatchedDim,
}

#[derive(Debug)]
pub struct WordVec(pub Vec<f64>);

impl WordVec {
    fn as_bytes(&self) -> Vec<u8> {
        let mut buf = Vec::with_capacity(size_of::<f32>() * self.0.len());
        for e in self.0.iter() {
            // write f64 as f32
            buf.write_f32::<LittleEndian>(*e as f32).unwrap();
        }

        return buf;
    }
}

use std::fmt;
impl fmt::Display for WordVec {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let WordVec(ref vec) = *self;

        for (count, v) in vec.iter().enumerate() {
            write!(f, "{:.7}", v)?;
            if count != vec.len() - 1 {
                write!(f, " ")?;
            }
        }

        Ok(())
    }
}

impl From<ParseFloatError> for ParseVecError {
    fn from(err: ParseFloatError) -> ParseVecError {
        ParseVecError::ParseFloat(err)
    }
}

fn parse_vector_line(line: &str, vector_size: usize) -> Result<(String, WordVec), ParseVecError> {
    let mut split = line.trim().split(" ");

    let word = split.next().unwrap().to_string();

    let vec: Vec<f64> = split.map(|e| e.parse::<f64>().unwrap()).collect();

    if vec.len() != vector_size {
        return Err(ParseVecError::MissmatchedDim);
    }

    Ok((word, WordVec(vec)))
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum OutputFormat {
    Binary,
    Text,
}

impl From<u8> for OutputFormat {
    fn from(u: u8) -> Self {
        match u {
            0 => OutputFormat::Binary,
            1 => OutputFormat::Text,
            _ => panic!("Invalid value for OutputFormat {:?}!", u),
        }
    }
}

impl From<OutputFormat> for String {
    fn from(output_format: OutputFormat) -> String {
        match output_format {
            OutputFormat::Binary => String::from("Binary"),
            OutputFormat::Text => String::from("Text"),
        }
    }
}

pub fn combine(
    dict: Arc<Dict>,
    vec_file: &str,
    vector_size: usize,
    word_weight: f32,
    save_mode: SaveMode,
    output_format: OutputFormat,
    out_dir: &str,
) -> Result<(), Error> {
    let vocab_size = dict.nwords as usize;
    println!("Vocab size: {:?}", vocab_size);

    let vector_size: usize = match save_mode {
        SaveMode::WithBias => vector_size + 1,
        SaveMode::WoutBias => vector_size,
    };
    println!("Vector size ({}): {}", String::from(save_mode), vector_size);

    let probs: Vec<f32> = dict.get_probabilities();

    println!("Combine...");
    let mut words: Vec<String> = vec![];
    let mut word_vecs: Vec<f64> = Vec::with_capacity(vocab_size * vector_size);
    {
        let input_vec_file = File::open(vec_file).unwrap();
        let reader = BufReader::with_capacity(5 * 1024 * 1024, input_vec_file);
        let mut ngram_vecs: FnvHashMap<String, WordVec> = FnvHashMap::default();
        for (i, line) in reader.lines().enumerate() {
            let line = line.unwrap();

            let (word, vec) = match parse_vector_line(&line, vector_size) {
                Ok((w, v)) => (w, v),
                Err(ParseVecError::MissmatchedDim) => {
                    println!("Skip line {}: {:?}", i + 1, line);
                    continue;
                }
                Err(ParseVecError::ParseFloat(err)) => panic!("{}:{:?}, {:?}", i + 1, err, line),
            };

            ngram_vecs.insert(word, vec);
        }

        // word ids start from 1
        for i in 1..=vocab_size {
            let e = dict.get_entry(i);
            if let Some(word) = e.get_word() {
                words.push(word.to_string());
                if vocab::is_uppercase(&word) {
                    let word_vec = ngram_vecs.get(&e.token).unwrap();
                    word_vecs.extend(word_vec.0.clone());
                } else {
                    let word_vec =
                        sif::combine(&e, &dict, word_weight, vector_size, &ngram_vecs, &probs);
                    word_vecs.extend(word_vec.0);
                };
            }
        }
        // ngram_vecs dropped here
    }

    assert!(
        vocab_size == word_vecs.len() / vector_size,
        "Vocab size {} doesn't match number of word-vectors {}",
        vocab_size,
        word_vecs.len() / vector_size
    );

    println!("Remove PC...");
    // remove the first common component
    let pc = sif::remove_pc(&mut word_vecs, vocab_size, vector_size);
    let word_vecs: Vec<WordVec> = word_vecs
        .chunks(vector_size)
        .map(|v| WordVec(v.to_vec()))
        .collect();

    println!("Combined {} word vectors", word_vecs.len());

    if output_format == OutputFormat::Binary {
        let out_word_file_path = format!("{}/words.txt", out_dir);
        println!("Saving words to: {}", &out_word_file_path);
        let mut words_file = File::create(&out_word_file_path)?;

        let out_vecs_file_path = format!("{}/vectors.bin", out_dir);
        println!("Saving vectors to: {}", &out_vecs_file_path);
        let mut vecs_file = File::create(&out_vecs_file_path)?;

        for (word, word_vec) in words.iter().zip(word_vecs.iter()) {
            words_file.write_all(format!("{}\n", word).as_bytes())?;
            vecs_file.write_all(&word_vec.as_bytes())?;
        }
    } else {
        let out_file_path = format!("{}/vectors.txt", out_dir);
        println!("Saving words and vectors to: {}", &out_file_path);
        let mut out_file = File::create(&out_file_path)?;

        for (word, word_vec) in words.iter().zip(word_vecs.iter()) {
            let line = format!("{} {}\n", word, word_vec);
            out_file.write_all(line.as_bytes())?;
        }
    }

    let out_pc_file_path = format!("{}/pc.txt", out_dir);
    println!(
        "Saving the 1st principal component to: {}",
        &out_pc_file_path
    );
    let mut out_file = File::create(&out_pc_file_path)?;
    let line = format!("1st_pc {}\n", pc);
    out_file.write_all(line.as_bytes())?;

    Ok(())
}
