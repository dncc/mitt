# Mitt

Mitt is a fast, scalable, open-source library for word-to-vector representations, implemented in Rust.

## Table of contents

* [Documentation](#documentation)
    * [Model Description](#model-description)
        * [Negative Sampling](#negative-sampling)
        * [Vector Representations For Character Ngrams](#vector-representations-for-character-ngrams)
        * [SIF](#sif)
        * [Vector Representations For Phrases](#vector-representations-for-phrases)
        * [Scaling](#scaling)
        * [Numerical Stabilization](#numerical-stabilization)
        * [Sub-sampling](#sub-sampling)
        * [New Distance Weight Function](#new-distance-weight-function)
    * [Command Line Interface](#command-line-interface)
* [Install](#install)
* [Training](#training)
* [References](#references)

## Documentation

### Model Description

`Mitt` learns vector representations for words from a given text corpus by using an extended and modified [GloVe](https://nlp.stanford.edu/pubs/glove.pdf)[^glove] model (hence the name). Extensions and modifications are as follows:


#### Negative sampling

Glove minimizes the following cost function:

```math
J = \Sigma_{i,k}{ f(X_{i,k}) (b_i + b_k + W_i^TW_k - log_e(X_{i,k}))^2 }
```

where $`W_i^T`$ is a transposed word vector for i-th word, $`W_k`$ is a word vector for its context word, $`log_e(X_{i,k})`$ is natural log of a co-occurrence count for $`i,k`$ word pair and $`f(X_{i,k})`$ is a weighting function defined as:

```math
f(x)= \begin{cases}
    (\frac{x}{x_{max}})^\alpha, & \text{if } x \lt x_{max}\\
    1, & \text{otherwise}
\end{cases}
```

whith $`x_{max}`$ and $`\alpha`$ as hyper-parameters, set by default to 100 and 0.75, respectively.

Mitt changes the cost function in order to include negatively sampled word-cooccurrece pairs, as follows:

```math
J = \Sigma_{i,k}{ f(X_{i,k}) (b_i - b_l + W_i^TW_k - log_e(X_{i,k}) - W_l^TW_k + log_e(X_{l,k}))^2 }
```

where $`W_l^T`$ and $`log_e(X_{l,k})`$ are a transposed word vector and co-occurrence count for a word $`l`$, obtained from negative sampling, such that $`log_e(X_{l,k}) \lt log_e(X_{i,k})`$ and $`W_i^TW_k \leq W_l^TW_k`$. This change in the cost function is derived from the underlying assumptions described in the Glove paper (section 3[^glove]).


#### Vector Representations For Character Ngrams

`Mitt` extends Glove similarly to how [`fastText`](https://fasttext.cc/) extends [`word2vec`](https://code.google.com/archive/p/word2vec/)[^vv2v], by learning character n-gram vectors and combining them to obtain word embeddings. With respect to this, `Mitt` is to `Glove` as `fastText` is to `word2vec`, or similar to the well known word-vector formula, `king - man + woman = queen`, one can write: `fastText - word2vec + Glove = Mitt`.[^fastText]

#### SIF

N-ngram vectors are combined by the SIF[^sif] procedure which increases resulting word-vectors accuracy on semantic tasks in comparison to a, more common, method of using an averaged sum of n-gram vectors. The SIF method consists of 2 steps:


* the first step combines linearly n-gram vectors with weights equal to: a / (a + p), where `p` is a word probability (word_frequencyᵢ / sum_of_all_word_frequencies) and `a` is a smoothing constant (usually set to a value in 1e-3...1e-5 range).

* the second step finds SVD of the word-vectors matrix and subtract the first principal component obtained from the vectors in the first step.


#### Vector Representations For Phrases

Like `word2phrase` from [`word2vec`](https://code.google.com/archive/p/word2vec/), `Mitt` learns phrases and their corresponding vector-representations. Phrases are created by concatenating frequently co-occurring words, i.e. words that have their [PMI](https://en.wikipedia.org/wiki/Pointwise_mutual_information) above a pre-determined threshold (100 is a default value).[^vv2v]

#### Scaling

Parallelizing creation of co-occurrence matrix was required in order to scale the model, as building a co-occurrence matrix of words and n-grams on a multi-billion word corpus in a single thread becomes infeasible.

#### Numerical Stabilization

Numerical stabilization of the learning algorithm was accomplished by dynamic switching from Adagrad to a variant of RMSProp, gradient clipping and parameter regularization.

* switching from Adagrad to RMSProp and gradient clipping was introduced to prevent gradient descent algorithm from blowing-up or diminishing gradients; training Glove model on a __large text__ corpus (>20B words) suffers from this and ultimately halts the learning process in a couple of iterations.

* parameter regularization was introduced to prevent numerical instability in the process of learning word vectors and it was done by the "Decoupled Weight Decay Regularization"[^regularization].


#### Sub-sampling

Random removal of frequent words with probability `p`: `p = 1 - √ (t / f)`, where `t` is a manually set frequency threshold and `f` is a word frequency.[^hyperparams]

#### New Distance Weight Function

Replacing Glove's harmonic distance weights with ones that decay more gradually.[^hyperparams]


These changes bring two improvements in comparison to the original Glove model:

1. The accuracy of learned vectors significantly increases, in both, syntactic and semantic accuracy tests. See accuracy comparison [^table] for more details.

2. Solution to the out-of-vocabulary problem: character n-gram vectors from known words (in-the-vocabulary) can be used to obtain vector-representations for unknown words (out-of-vocabulary) that are made of the same character n-grams.

### Command Line Interface

User could specify the following parameters:

| Short |   Full          |  Description                                      |
|:------|:----------------|:--------------------------------------------------|
| -i    | --input <input> | Input file, text corpus used to train the model   |
| -f    | --min-freq <min_freq>  | Minimum frequency of the vocabulary words, a threshold that determines which words are included in the vocabulary   |
| -w    | --window <window> |                        The size of the context word-window |
| -s    | --symmetric <symmetric> |                  Use symmetric context (exchange roles of word1 and word2), when counting co-occurrences |
|       | --xmax <xmax> |  Glove weighting function param, scaling value                            |
|       | --alpha <alpha> |  Glove weighting function param, power value                            |
| -o    | --out <out> |                              Output file |

_Unlike_ the [GloVe implementation](https://github.com/stanfordnlp/GloVe), the user could specify the following additional parameters:

| Short |   Full          |  Description                                      |
|:------|:----------------|:--------------------------------------------------|
|       | --minn <ngram_len> |                   Minimum length for character n-grams. |
|       | --maxn <ngram_len> |                   Maximum length for character n-grams. |
|       | --word-dist-weight <word_dist_weight> |   Decrease word co-occurrence counts based on the their distance. |
|       | --ngram-dist-weight <ngram_dist_weight> |   Decrease n-gram co-occurrence counts based on the their distance. |
|       | --word-subsampling <word_subsampling> |         Use word sub-sampling to omit frequent words. |
|       | --ngram-subsampling <ngram_subsampling> |   Use character n-gram sub-sampling to omit frequent sub-words. |
|       | --skip-threshold <skip_threshold> |   Probability threshold for word and ngram subsampling. |
| -p    | --workers <workers> |                       Number of parallel threads. |

## Install

Running `make install` will make a release project build and copy the binary to the `/usr/local/bin/mitt`; for development: `make build` will build the project and create a symlink from the built binary to the root of the project.

Then, typing `./mitt help` should give the following output:

```
mitt:

USAGE:
    mitt [OPTIONS] [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
        --config <config>    Path to the config file.

SUBCOMMANDS:
    combine           Combines word and character ngram vectors
    cooccur           Creates table of word co-occurrences
    help              Prints this message or the help of the given subcommand(s)
    merge             Merges co-occurrence shards to one binary file
    ngrams-cooccur    Create table of ngrams co-occurrences
    phrases           Build vocabulary with phrases
    shuffle           Shuffles table of word or/and ngram co-occurrences
    train             Trains model
    vocab             Builds vocabulary
```

## Run


## References

[^vv2v]: T. Mikolov, I. Sutskever, K. Chen, G. Corrado, J. Dean, [Distributed Representations of Words and Phrases and their Compositionality](https://papers.nips.cc/paper/5021-distributed-representations-of-words-and-phrases-and-their-compositionality.pdf)
[^glove]: J. Pennington,  R. Socher,  C. D. Manning, [GloVe: Global Vectors for Word Representation](https://nlp.stanford.edu/pubs/glove.pdf)
[^sif]: S. Arora, Y. Liang, T. Ma, [A Simple  but tough-to-beat baseline for sentence embeddings](https://openreview.net/pdf?id=SyK00v5xx)
[^hyperparams]: O. Levy, Y. Goldberg, I. Dagan, [Improving Distributional Similarity with Lessons Learned from Word Embeddings](https://levyomer.files.wordpress.com/2015/03/improving-distributional-similarity-tacl-2015.pdf)
[^fastText]: P. Bojanowski*, E. Grave*, A. Joulin, T. Mikolov, [Enriching Word Vectors with Subword Information](https://arxiv.org/abs/1607.04606)
[^regularization]: I. Loshchilov, F. Hutter, [Decoupled Weight Decay Regularization](https://arxiv.org/abs/1711.05101)
